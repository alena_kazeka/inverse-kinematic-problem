﻿using System.Collections.Generic;
using System;
using System.Windows.Media.Media3D;

namespace DataConverterLib
{
    /// <summary>
    /// Статический класс, предоставляющий методы для преобразования заданных фигур в набор точек
    /// </summary>
    public static class TrajectoryConverter
    {
        /// <summary>
        /// Движение по линии
        /// </summary>
        /// <param name="start">Начальная точка</param>
        /// <param name="end">Конечная точка</param>
        /// <param name="pointsCount">Число промежуточных точек</param>
        /// <returns>Список промежуточных точек</returns>
        public static List<Point3D> LineToPoints(Point3D start, Point3D end, int pointsCount = 10)
        {
            if (start == null || end == null)
                throw new NullReferenceException();

            //double multiplier = Math.Sqrt(Math.Pow(start.X - end.X, 2) + Math.Pow(start.Y - end.Y, 2) + Math.Pow(start.Z - end.Z, 2)) / pointsCount;

            Vector3D vector = new Vector3D(end.X - start.X, end.Y - start.Y, end.Z - start.Z);
            Vector3D add = vector / pointsCount;

            List<Point3D> lineToPoints = new List<Point3D>();
            Point3D nextPoint = start;

            for (int i = 0; i < pointsCount; i++)
            {
                nextPoint += add;
                lineToPoints.Add(nextPoint);
            }

            return lineToPoints;
        }

        /// <summary>
        /// Движение по восьмерке
        /// </summary>
        /// <param name="start">Начальная точка</param>
        /// <param name="pointsCount">Число промежуточных точек</param>
        /// <returns>Список промежуточных точек</returns>
        public static List<Point3D> InfinitySymbolToPoints(Point3D start, int pointsCount = 10)
        {
            if (start == null)
                throw new NullReferenceException();

            Point3D nextPoint;
            double add;
            double multiplier = 4.0 * Math.PI / pointsCount;

            List<Point3D> infSymbolToPoints = new List<Point3D>();
            
            for (int i = 0; i < pointsCount; i++)
            {                
                if (i < pointsCount / 2)
                {
                    add = multiplier* i;
                    nextPoint = new Point3D(start.X, start.Y + Math.Sin(add), start.Z + add);
                }
                else
                {
                    add = multiplier * (pointsCount - i);
                    nextPoint = new Point3D(start.X, start.Y + Math.Sin(-add), start.Z + add);
                }
                infSymbolToPoints.Add(nextPoint);
            }

            return infSymbolToPoints;
        }
    }
}
