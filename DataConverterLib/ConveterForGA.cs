﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataConverterLib
{
    public static class ConveterForGA
    {
        /// <summary>
        /// конвертировать List типа double  в массив char
        /// </summary>
        /// <param name="values">список значений типа double</param>
        /// <param name="intPartLength">количество разрядов под целую часть числа (360 градусов помещается в 9 разрядов)</param>
        /// <param name="fractPartLength">количество разрядов под вещественную часть числа (30 => 999 999 999)</param>
        /// <returns></returns>
        public static char[] ThetasListToCharArray(List<double> values, int intPartLength = 9, int fractPartLength = 30)
        {
            StringBuilder builder = new StringBuilder();
            String intPart, fractPart;
            int zerosAddCount = 0;
            foreach (double value in values)
            {
                intPart = Convert.ToString(integerPart(value), 2);
                fractPart = Convert.ToString(fractialPart(value), 2);
                zerosAddCount = intPartLength - intPart.Length;
                if (zerosAddCount > 0)
                    for (int i = 0; i < zerosAddCount; i++)
                        builder.Append('0');
                builder.Append(intPart);
                zerosAddCount = fractPartLength - fractPart.Length;
                if (zerosAddCount > 0)
                    for (int i = 0; i < zerosAddCount; i++)
                        builder.Append('0');
                builder.Append(fractPart);
            }
            char[] array = builder.ToString().ToCharArray();
            return array;
        }

        private static int integerPart(double value)
        {
            return (int)value;
        }

        private static int fractialPart(double value)
        {
            value *= 1.0;
            string strValue = String.Format("{0:0.#########}", value);
            string[] parts = strValue.Split(',', '.');
            if (parts.Length < 2)
                return 0;

            int fractial;
            if (int.TryParse(parts[1], out fractial))
                return fractial;

            return 0;
        }

        public static List<double> CharArrayToThetasList(char[] array, int intPartLength = 9, int fractPartLength = 30)
        {
            int numberLength = intPartLength + fractPartLength;
            int listLength = array.Length / numberLength;
            List<double> list = new List<double>();
            int intPart, fractPart;
            String values = new String(array);
            String value;
            double requiredValue = 0;
            for (int i = 0; i < listLength; i++)
            {
                intPart = Convert.ToInt32(values.Substring(i * numberLength, intPartLength), 2);
                fractPart = Convert.ToInt32(values.Substring(i * numberLength + intPartLength, fractPartLength), 2);
                value = intPart + "," + fractPart;
                if (Double.TryParse(value, out requiredValue))
                    list.Add(requiredValue);
            }

            return list;
        }
    }
}
