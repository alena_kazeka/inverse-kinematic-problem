﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using StatisticsGALib;
using System.Text;

namespace GeneticAlgorithmsLib
{
    namespace HillClimbingGALib
    {
        public delegate double Fitness(char[] genes);

        /// <summary>
        /// Класс HillClimbingGA реализует алгоритм Hill Climbing,
        /// суть которого мутация единственной хромосомы
        /// </summary>
        public class HillClimbingGA
        {
            /// <summary>
            /// хромосома
            /// </summary>
            Chromosome chromosome;

            /// <summary>
            /// конструктор класса HillClimbingGA
            /// </summary>
            /// <param name="chromosomeLength">длина хромосомы</param>
            /// <param name="fitness">реализация фитнесс-функции</param>
            public HillClimbingGA(int chromosomeLength, Fitness fitness)
            {
                chromosome = new Chromosome(chromosomeLength, fitness);
                chromosome.randomInitialize();
                Generations = new List<char[]>();
                Generations.Add(chromosome.Genes);
            }

            public HillClimbingGA(Chromosome chromosome)
            {
                this.chromosome = chromosome;
                Generations = new List<char[]>();
                Generations.Add(chromosome.Genes);
            }

            /// <summary>
            /// мутация хромосомы
            /// </summary>
            /// <param name="probability">вероятность мутации каждого гена</param>
            /// <returns></returns>
            private Chromosome mutate(double probability)
            {
                Random rand = new Random(DateTime.Now.Millisecond);
                Chromosome mutatedChromosome;
                double chromFitness = 0, mutatedchromFitness = 0;
                int iterationsCount = 0;
                do
                {
                    mutatedChromosome = new Chromosome(chromosome.Genes, chromosome.fitness);
                    for (int i = 0; i < mutatedChromosome.Genes.Length; i++)
                    {
                        if (rand.NextDouble() <= probability)
                        {
                            if (mutatedChromosome[i] == '1')
                                mutatedChromosome[i] = '0';
                            else
                                mutatedChromosome[i] = '1';
                        }
                    }
                    chromFitness = chromosome.fitness(chromosome.Genes);
                    mutatedchromFitness = mutatedChromosome.fitness(mutatedChromosome.Genes);
                    iterationsCount++;
                }
                while (chromFitness <= mutatedchromFitness && iterationsCount < 40);
                if (iterationsCount == 40)
                     return chromosome;
                return mutatedChromosome;
            }

            private Chromosome shake()
            {
                Random rand = new Random(DateTime.Now.Millisecond + 20);
                Chromosome mutatedChromosome;
             //   char[] randChrom = new char[chromosome.Genes.Length];
                mutatedChromosome = new Chromosome(chromosome.Genes, chromosome.fitness);

                for (int i = 0; i < mutatedChromosome.Genes.Length; i++)
                {
                    //if (rand.NextDouble() <= 0.4)
                    //    randChrom[i] = '1';
                    //else
                    //    randChrom[i] = '0';
                    if (rand.NextDouble() <= 0.3)
                    {
                        if (mutatedChromosome[i] == '1')
                            mutatedChromosome[i] = '0';
                        else
                            mutatedChromosome[i] = '1';
                    }
                }
             //   mutatedChromosome = new Chromosome(randChrom, chromosome.fitness);
                return mutatedChromosome;
            }

            public List<char[]> Generations { get; set; }

            /// <summary>
            /// эволюционирование
            /// </summary>
            /// <param name="probability">вероятность мутации</param>
            /// <returns>мутировавшая хромосома</returns>
            public char[] Evolve(double probability, double accuracy)
            {
                int i = 0;

                Stopwatch stopwatch = new Stopwatch();
                Statistics.ErrorsGA.Clear();
                stopwatch.Start();
                double fit = chromosome.fitness(chromosome.Genes);
                Statistics.ErrorsGA.Add(fit);
                Generations.Add(chromosome.Genes);
                double prevFit;
                int counterTheSameFit = 0;
                while (fit > accuracy && i < 1500 /*&& counterTheSameFit < 200*/)
                {
                    chromosome = mutate(probability);
                    i++;
                    prevFit = fit;
                    fit = chromosome.fitness(chromosome.Genes);

                    if (prevFit == fit)
                        counterTheSameFit++;
                    else
                        counterTheSameFit = 0;

                    if (counterTheSameFit == 150)
                        chromosome = shake();

                    if (i >= 500 && i % 500 == 0)
                        chromosome = shake();

                    Generations.Add(chromosome.Genes);
                    Statistics.ErrorsGA.Add(fit);
                }
                stopwatch.Stop();
                Statistics.SaveIterationsStatistics(i, stopwatch.Elapsed.TotalMilliseconds, fit);

                return chromosome.Genes;
            }
        }

        public class Chromosome
        {
            /// <summary>
            /// гены хромосомы
            /// </summary>
            private char[] genes;

            public char[] Genes
            {
                get { return (char[])genes.Clone(); }
                set { genes = value; }
            }

            public char this[int index]
            {
                get { return genes[index]; }
                set { genes[index] = value; }
            }

            /// <summary>
            /// фитнесс-функция
            /// </summary>

            internal Fitness fitness;

            /// <summary>
            /// конструктор класса Chromosome
            /// </summary>
            /// <param name="length">длина хромосомы</param>
            /// <param name="fitness">финтнесс-функция</param>
            public Chromosome(int length, Fitness fitness)
            {
                this.fitness = fitness;
                genes = new char[length];
            }

            internal void randomInitialize()
            {
                Random rand = new Random(DateTime.Now.Millisecond);
                for (int i = 0; i < genes.Length; i++)
                    genes[i] = (char)rand.Next(0, 2);
            }

            /// <summary>
            /// конструктор класса Chromosome
            /// </summary>
            /// <param name="genes">гены хромосомы</param>
            /// <param name="fitness">фитнесс-функция</param>
            public Chromosome(char[] genes, Fitness fitness)
            {
                this.genes = genes;
                this.fitness = fitness;
            }

            /// <summary>
            /// переопределение функции проверки на равенство
            /// </summary>
            /// <param name="obj"></param>
            /// <returns></returns>
            public override bool Equals(Object obj)
            {
                if (obj == null)
                    return false;

                Chromosome chromosome = obj as Chromosome;
                if (chromosome == null)
                    return false;

                return genes.Equals(chromosome.genes);
            }
        }
    }
}
