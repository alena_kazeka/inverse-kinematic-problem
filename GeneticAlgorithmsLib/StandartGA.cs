﻿using System;
using System.Diagnostics;
using StatisticsGALib;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GeneticAlgorithmsLib
{
    public delegate double Fitness(double[] chromosome);

    /// <summary>
    /// Стандартный генетический алгоритм с дискретной рекомбинацией
    /// </summary>
    public class StandartGA
    {
        /// <summary>
        /// популяция
        /// </summary>
        protected double[][] population;

        /// <summary>
        /// ограничения для генерации хромосом
        /// </summary>
        protected double[][] restrictions;

        /// <summary>
        /// Конструктор класса StandartGA
        /// </summary>
        /// <param name="populationSize">размер популяции</param>
        /// <param name="chromosomeLength">длина хромосомы</param>
        /// <param name="restrictions">ограничения на инициализацию хромосом. в данном случае - минимальное и максимальное значение углов</param>
        /// <param name="fitness">функция приспособленности</param>
        public StandartGA(int populationSize, int chromosomeLength, double[][] restrictions, Fitness fitness)
        {
            population = new double[populationSize][];
            for (int i = 0; i < populationSize; i++)
                population[i] = new double[chromosomeLength];

            this.restrictions = restrictions;
            this.fitness = fitness;
            randomInitialize();
            sortPopulation();

            Generations = new List<double[]>();
        }


        /// <summary>
        /// Конструктор класса StandartGA
        /// </summary>
        /// <param name="populationSize">Размер популяции</param>
        /// <param name="chromosomeLength">Длина хромосомы</param>
        /// <param name="firstPopulation">Популяция для инициализации первой популяции или ее части</param>
        /// <param name="restrictions">Ограничения на инициализацию хромосом</param>
        /// <param name="fitness">Функция приспособленности</param>
        public StandartGA(int populationSize, int chromosomeLength, double[][] firstPopulation, double[][] restrictions, Fitness fitness)
        {
            population = new double[populationSize][];
            for (int i = 0; i < population.Length; i++)
            {
                population[i] = new double[chromosomeLength];
                if (i < firstPopulation.Length)
                {
                    for (int j = 0; j < chromosomeLength; j++)
                        population[i][j] = firstPopulation[i][j];
                }
                else
                {
                    for (int j = 0; j < population[i].Length; j++)
                        population[i][j] = restrictions[0][j] + rand.NextDouble() * (restrictions[1][j] - restrictions[0][j]);
                }
            }

            this.restrictions = restrictions;
            this.fitness = fitness;
            sortPopulation();

            Generations = new List<double[]>();
        }
        /// <summary>
        /// случайная инициализация популяции в пределах допустимых значений
        /// </summary>
        protected void randomInitialize()
        {
          //  Random rand = new Random(DateTime.Now.Millisecond);
            for (int i = 0; i < population.Length; i++)
                for (int j = 0; j < population[i].Length; j++)
                    population[i][j] = restrictions[0][j] + rand.NextDouble() * (restrictions[1][j] - restrictions[0][j]);
        }

        protected Fitness fitness;

        /// <summary>
        /// сортировка популяции в соответствии с функцией принадлежности
        /// </summary>
        protected void sortPopulation()
        {
            double[] keys = new double[population.Length];
            int[] items = new int[population.Length];
            for (int i = 0; i < population.Length; i++)
            {
                keys[i] = fitness(population[i]);
                items[i] = i;
            }

            Array.Sort(keys, items);

            double[][] sortedPopulation = new double[population.Length][];
            for (int i = 0; i < population.Length; i++)
                sortedPopulation[i] = (double[])population[items[i]].Clone();

            population = sortedPopulation;
        }

        /// <summary>
        /// определение следующего поколения
        /// </summary>
        protected void Select()
        {
          //  Random rand = new Random(DateTime.Now.Millisecond);
            double[][] newPopulation = new double[population.Length][];
            Parallel.For(0, population.Length, (i) =>
            {
                double[] mama = population[rand.Next(population.Length / 2)];
                double[] dad = population[rand.Next(population.Length / 2)];
                double[] child = IntermediateRecombination(mama, dad);
                newPopulation[i] = child;
            });
            Array.Copy(newPopulation, population, newPopulation.Length);
            sortPopulation();
        }

        protected Random rand = new Random(DateTime.Now.Millisecond);

        /// <summary>
        /// промежуточная рекомбинация: потомок = родитель1 + alpha*(родитель2 - родитель1)
        /// </summary>
        /// <param name="mama">первый родитель</param>
        /// <param name="dad">второй родитель</param>
        /// <returns>потомок</returns>
        protected double[] IntermediateRecombination(double[] mama, double[] dad)
        {
            int length = mama.Length;
            double[] firstChild = new double[length];
            double[] secondChild = new double[length];

            double currentValue, alpha;
            for (int i = 0; i < length; i++)
            {
                //устанавливаем занчения генов потомков
                do
                {
                    if (rand.Next(0, 2) == 1)
                        alpha = rand.NextDouble() * 1.5;
                    else
                        alpha = rand.NextDouble() * (-0.25);

                    currentValue = mama[i] + alpha * (dad[i] - mama[i]);
                } while (currentValue < restrictions[0][i] || currentValue > restrictions[1][i]);
                firstChild[i] = currentValue;

                do
                {
                    if (rand.Next(0, 2) == 1)
                        alpha = rand.NextDouble() * 1.5;
                    else
                        alpha = rand.NextDouble() * (-0.25);

                    currentValue = dad[i] + alpha * (mama[i] - dad[i]);
                } while (currentValue < restrictions[0][i] || currentValue > restrictions[1][i]);
                secondChild[i] = currentValue;
            }
            double firstChildFitness = fitness(firstChild);
            double seconChildFitness = fitness(secondChild);
            double mamaFitness = fitness(mama);
            double dadFitness = fitness(dad);


            if (firstChildFitness < seconChildFitness)
            {
                //if (firstChildFitness < mamaFitness && firstChildFitness < dadFitness)
                    return firstChild;
            }
            else
            {
               // if (seconChildFitness < mamaFitness && seconChildFitness < dadFitness)
                    return secondChild;
            }

            //if (mamaFitness < dadFitness)
            //    return mama;
            //else
            //    return dad;
        }

        public List<double[]> Generations { get; set; }


        /// <summary>
        /// эволюция
        /// </summary>
        /// <returns>лучшая особь</returns>
        public virtual double[] Evolve(double accuracy)
        {
            int iterations = 0;

            Stopwatch stopwatch = new Stopwatch();
            Statistics.ErrorsGA.Clear();
            stopwatch.Start();
            double bestFitness = fitness(population[0]);
            Statistics.ErrorsGA.Add(bestFitness);
            while (bestFitness > accuracy && iterations < 1000)
            {
                Select();
                bestFitness = fitness(population[0]);
                iterations++;

                Generations.Add((double[])population[0].Clone());
                Statistics.ErrorsGA.Add(bestFitness);

                //встряска
                if (iterations > 100 && iterations % 200 == 0)
                {
                    randomInitialize();
                    sortPopulation();
                }
            }
            stopwatch.Stop();
            Statistics.SaveIterationsStatistics(iterations, stopwatch.Elapsed.TotalMilliseconds, bestFitness);

            return population[0];
        }
    }
}
