﻿namespace GeneticAlgorithmsLib
{
    /// <summary>
    /// Стандартный генетический алгоритм с дискретной рекомбинацией
    /// </summary>
    public class StandartGaDynamic: StandartGA
    {
        /// <summary>
        /// Конструктор класса StandartGaDynamic
        /// </summary>
        /// <param name="populationSize">размер популяции</param>
        /// <param name="chromosomeLength">длина хромосомы</param>
        /// <param name="restrictions">ограничения на инициализацию хромосом. в данном случае - минимальное и максимальное значение углов</param>
        /// <param name="fitness">функция приспособленности</param>
        public StandartGaDynamic(int populationSize, int chromosomeLength, double[][] restrictions, Fitness fitness): base(populationSize, chromosomeLength, restrictions, fitness)
        {
        }

        /// <summary>
        /// Конструктор класса StandartGaDynamic
        /// </summary>
        /// <param name="populationSize">Размер популяции</param>
        /// <param name="chromosomeLength">Длина хромосомы</param>
        /// <param name="firstPopulation">Популяция для инициализации первой популяции или ее части</param>
        /// <param name="restrictions">Ограничения на инициализацию хромосом</param>
        /// <param name="fitness">Функция приспособленности</param>
        public StandartGaDynamic(int populationSize, int chromosomeLength, double[][] firstPopulation, double[][] restrictions, Fitness fitness) : base(populationSize, chromosomeLength, firstPopulation, restrictions, fitness)
        {
        }
       
        /// <summary>
        /// эволюция
        /// </summary>
        /// <returns>лучшая особь</returns>
        override public double[] Evolve(double accuracy)
        {
            int iterations = 0;
            
            double bestFitness = fitness(population[0]);

            while (bestFitness > accuracy && iterations < 350)
            {
                Select();
                bestFitness = fitness(population[0]);
                iterations++;

                //встряска
                if (iterations >= 100 && iterations % 100 == 0)
                {
                    randomInitialize();
                    sortPopulation();
                }
            }

            if (iterations == 350)
                return null;

            return population[0];
        }
    }
}

