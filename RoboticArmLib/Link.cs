﻿using System.Windows.Media.Media3D;
using System;

namespace RoboticArmLib
{
    /// <summary>
    /// Описывает звено манипулятора. Текущий вариант без вращения звеньев (двумерное пространство)
    /// </summary>
    public class Link
    {
        /// <summary>
        /// длина звена
        /// </summary>
        private double length;
        public double Length { get { return length; } }

        /// <summary>
        /// текущий угол наклона в градусах
        /// </summary>
        private double theta;
        public double Theta { get { return theta; } set { theta = value; } }

        /// <summary>
        /// минимальный угол
        /// </summary>
        private double minTheta;
        public double MinTheta { get { return minTheta; } }

        /// <summary>
        /// максимальный угол
        /// </summary>
        private double maxTheta;
        public double MaxTheta { get { return maxTheta; } }

        /// <summary>
        /// точка начала соединения (конец предыдущего звена)
        /// </summary>
        private Point3D startPoint;
        public Point3D StartPoint
        {
            get { return startPoint; }
            set { startPoint = value; }
        }

        /// <summary>
        /// конструктор класса Link
        /// </summary>
        /// <param name="length">длина звена</param>
        /// <param name="theta">угол наклона</param>
        /// <param name="minTheta">минимальный угол наклона</param>
        /// <param name="maxTheta">максимальный угол наклона</param>
        public Link(double length, double theta, double minTheta = 0, double maxTheta = 360)
        {
            if (length < 0)
                throw new ArgumentException("Длина звена должна быть положительным числом.");
            this.length = length;
            this.theta = theta;
            if (minTheta > maxTheta)
                throw new ArgumentException("Минимальный угол наклона должен быть меньше максимального угла наклона.");
            if (theta > maxTheta && theta < minTheta)
                throw new ArgumentException("Значение текущего угла наклона звена должно находиться в пределах [мин. угол, макс. угол].");          
            this.minTheta = minTheta;
            this.maxTheta = maxTheta;
        }

        /// <summary>
        /// представление звена в виде вектора
        /// </summary>
        public Vector3D Vector2D(double prevTheta)
        {
            return new Vector3D(length * Math.Cos((theta + prevTheta) * Math.PI / 180),
                                    length * Math.Sin((theta + prevTheta) * Math.PI / 180),
                                    0);
        }
        
        /// <summary>
        ///угол вращения в градусах 
        /// </summary>
        public double RotatingAngle
        {
            get; set;
        }

        public Vector3D RotatedVector(double prevTheta)
        {
            double x = length * Math.Cos((theta + prevTheta) * Math.PI / 180);
            double y = length * Math.Sin((theta + prevTheta) * Math.PI / 180);
           
            double _x = x * Math.Cos(-RotatingAngle * Math.PI / 180);
            double z = -x * Math.Sin(-RotatingAngle* Math.PI / 180);

            return new Vector3D(_x, y, z);          
        }
    }
}
