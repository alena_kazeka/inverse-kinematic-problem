﻿using System.Collections.Generic;
using System;
using System.Windows.Media.Media3D;

namespace RoboticArmLib
{
    /// <summary>
    /// Описывает руку-манипулятора
    /// </summary>
    public class HandManipulator
    {
        /// <summary>
        /// звенья руки-манипулятора
        /// </summary>
        private List<Link> links;

        public List<Link> Links
        {
            get { return links; }
        }

        public List<Link> LinksCopy
        {
            get
            {
                List<Link> copyLinks = new List<Link>();
                foreach (Link link in links)
                {
                    copyLinks.Add(new Link(link.Length, link.Theta, link.MinTheta, link.MaxTheta) { StartPoint = link.StartPoint });
                    copyLinks[copyLinks.Count - 1].RotatingAngle = link.RotatingAngle;
                }
                return copyLinks;
            }
        }

        /// <summary>
        /// конструктор класса HandManipulator
        /// </summary>
        public HandManipulator()
        {
            links = new List<Link>();
        }

        /// <summary>
        /// конструктор класса HandManipulator
        /// </summary>
        /// <param name="links">список звеньев манипулятора</param>
        public HandManipulator(List<Link> links)
        {
            if (links == null)
                throw new ArgumentNullException();

            this.links = links;
        }

        /// <summary>
        /// добавление звеньев к руке-манипулятору
        /// </summary>
        /// <param name="handmanipulator">объект класса HandManioulator</param>
        /// <param name="link">присоединяемое звено</param>
        /// <returns>объект класса HandManipulator с добавленным звеном</returns>
        public static HandManipulator operator +(HandManipulator handmanipulator, Link link)
        {
            if (handmanipulator == null || link == null)
                throw new ArgumentNullException();

            if (handmanipulator.Links.Count != 0)
                link.StartPoint = handmanipulator.EndPoint2D;
            
            handmanipulator.Links.Add(link);
            return new HandManipulator(handmanipulator.Links);
        }

        public void Add(Link link)
        {
            if (link == null)
                throw new ArgumentNullException();

            if (Links.Count != 0)
                link.StartPoint = EndPoint2D;

            Links.Add(link);
        }

        public void RemoveAt(int index)
        {
            if (index < 0 || index > links.Count - 1)
                throw new ArgumentException("Нормер звена должен находиться в пределах [0," + (links.Count - 1) + "].");

            List<Link> newLinks = new List<Link>();
            for (int i = 0; i < links.Count; i++)
            {
                if (i != index)
                    newLinks.Add(links[i]);
            }

            links.Clear();
            newLinks[0].StartPoint = new Point3D(0.0, 0.0, 0.0);
            foreach (Link link in newLinks)
                this.Add(link);
        }

        public void ChangeAt(int index, Link link)
        {
            if (index < 0 || index > links.Count - 1)
                throw new ArgumentException("Нормер звена должен находиться в пределах [0," + (links.Count - 1) + "].");

            List<Link> newLinks = new List<Link>();
            for (int i = 0; i < links.Count; i++)
            {
                if (i != index)
                    newLinks.Add(links[i]);
                else
                    newLinks.Add(link);
            }

            links.Clear();
            foreach (Link newlink in newLinks)
                this.Add(newlink);
        }
        
        /// <summary>
        /// Конечная точка руки-манипулятора
        /// </summary>
        public Point3D EndPoint2D
        {
            get { return endPoint(Links, Links.Count - 1); }
        }

        public Point3D EndPoint3D
        {
            get
            {
                double thetasSum;
                for (int i = 1; i <= links.Count; i++)
                {
                    thetasSum = 0;
                    for (int j = 0; j < i - 1; j++)
                        thetasSum += links[j].Theta;

                    if (i == links.Count)
                        return links[links.Count - 1].StartPoint + links[links.Count - 1].RotatedVector(thetasSum);

                    links[i].StartPoint = links[i - 1].StartPoint + links[i - 1].RotatedVector(thetasSum);
                }
                return new Point3D(0, 0, 0);

                //    return rotatePoint(EndPoint2D, -links[0].RotatingAngle);
            }
        }

        private Point3D rotatePoint(Point3D point, double phi)
        {
            Point3D newPoint = new Point3D(); 
            newPoint.X = point.X * Math.Cos(phi) + point.Z * Math.Sin(phi);
            newPoint.Z = -point.X * Math.Sin(phi) + point.Z * Math.Cos(phi);
            newPoint.Y = point.Y;
            return newPoint;
        }

        /// <summary>
        /// получить конечную точку для конкретного звена руки-манипулятора
        /// </summary>
        /// <param name="links">звенья</param>
        /// <param name="pos">номер звена</param>
        /// <returns></returns>
        private Point3D endPoint(List<Link> links, int pos)
        {
            if (links == null)
                throw new ArgumentNullException();
            if (pos < 0 || pos >= links.Count)
                throw new ArithmeticException();

            if (pos == 0)
                return links[pos].StartPoint + links[pos].Vector2D(0);
            else
            {
                double thetasSum = 0;
                for (int i = 0; i < pos; i++)
                    thetasSum += links[i].Theta;
                return links[pos].StartPoint + links[pos].Vector2D(thetasSum);
            }
        }

        /// <summary>
        /// изменить углы наклона звеньев
        /// </summary>
        /// <param name="thetas">новые углы наклонов</param>
        /// <returns>изменились ли углы (лежат ли новые углы в приемлимом диапазоне)</returns>
        public bool ChangeThetas(List<double> thetas)
        {
            for (int i = 0; i < thetas.Count; i++)
                if (thetas[i] > links[i].MaxTheta || thetas[i] < links[i].MinTheta)
                    return false;

            for (int i = 0; i < thetas.Count; i++)
            {
                if (thetas[i] > 360)
                    thetas[i] -= 360;

                links[i].Theta = thetas[i];
                if (i != 0)
                    links[i].StartPoint = endPoint(links, i - 1);
            }
            return true;
        }

        /// <summary>
        /// повернуть манипулятор на заданный угол
        /// </summary>
        public void Rotate()
        {
            //пересчет начальной точки для каждого звена
            double thetasSum;
            for (int i = 1; i < links.Count; i++)
            {
                thetasSum = 0;
                for (int j = 0; j < i-1; j++)
                    thetasSum += links[j].Theta;
                links[i].StartPoint = links[i-1].StartPoint + links[i-1].RotatedVector(thetasSum);
            }
        }


        public List<double> Thetas
        {
            get
            {
                List<double> thetas = new List<double>();
                foreach (Link link in links)
                    thetas.Add(link.Theta);

                return thetas;
            }
        }


        /// <summary>
        /// добавить угол поворота манипулятора к каждому звену
        /// </summary>
        /// <param name="phi">угол поворота манипулятора</param>
        public void AddRotatingAngle(double phi)
        {
            for (int i = 0; i < this.Links.Count; i++)
               this.Links[i].RotatingAngle = phi;
        }


        public double[][] GetRestrictions()
        {
            double[][] restriction = new double[2][];
            restriction[0] = new double[this.Links.Count];
            restriction[1] = new double[this.Links.Count];
            for (int i = 0; i < this.Links.Count; i++)
            {
                restriction[0][i] = this.Links[i].MinTheta;
                restriction[1][i] = this.Links[i].MaxTheta;
            }

            return restriction;
        }
    }
}
