﻿using System;
using System.Collections.Generic;
using System.IO;

namespace StatisticsGALib
{
    public static class Statistics
    {
        private static string filename = @".\statisticsGA.txt";
        private static StreamWriter writer;
        private static StreamReader reader;

        /// <summary>
        /// сохранить статистику работы генетического алгоритма в файл
        /// </summary>
        /// <param name="iterCount">число итераций</param>
        /// <param name="totalMilliseconds">время работы алгоритма в миллисекундах</param>
        public static void SaveIterationsStatistics(int iterCount, double totalMilliseconds, double error)
        {
            LastExecutionTime = totalMilliseconds / 1000;
            writer = new StreamWriter(filename);
            writer.WriteLine("итераций: " + iterCount);
            writer.WriteLine("время: " + totalMilliseconds/1000 + " с");
            writer.WriteLine("одна итерация: " + string.Format("{0:0.00}", iterCount == 0 ? 0 : (totalMilliseconds / iterCount)) + " мс");
            writer.WriteLine("ошибка: " + string.Format("{0:0.00}", error *10) + " мм");
            writer.Close();
        }

        /// <summary>
        /// последнее время выполнения алгоритма в секундах
        /// </summary>
        public static double LastExecutionTime = 0;

        public static string IterationsStatistics()
        {
            reader = new StreamReader(filename);
            string text = reader.ReadToEnd();
            reader.Close();
            return text;
        }

        private static List<double> errorsGA = new List<double>();

        public static List<double> ErrorsGA
        {
            get { return errorsGA; }
            set { errorsGA = value; }
        }

        public static void SaveDynamicStatistic(double totalMilliseconds, int recalculationCount, int notImprove)
        {
            writer = new StreamWriter(filename);
            writer.WriteLine("время: " + string.Format("{0:0.000}", totalMilliseconds / 1000) + " c");
            writer.WriteLine(/*"число обращений к словарю: "*/"пересчетов: " + recalculationCount);
            writer.WriteLine("результат не улучшен: " + notImprove);

            writer.Close();
        }
    }
}
