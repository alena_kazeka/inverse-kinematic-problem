﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoboticArmWpf
{
    namespace ManipulatorParameters
    {
        class ParametersException : Exception
        {
            public ParametersException(string message) : base(message) { }
        }
    }
}
