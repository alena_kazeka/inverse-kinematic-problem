﻿using System;
using System.Collections.Generic;
using System.Windows;
using RoboticArmLib;

namespace RoboticArmWpf
{
    namespace ManipulatorParameters
    {
        /// <summary>
        /// Interaction logic for ManipulatorParameters.xaml
        /// </summary>
        public partial class ManipulatorParametersWindow : Window
        {
            public ManipulatorParametersWindow(List<Link> links, double acc = 0.005)
            {
                InitializeComponent();
                manipulator = new HandManipulator(links);
                this.accurasy = acc;
                setParametres();
            }

            private HandManipulator manipulator;

            private double accurasy;

            public HandManipulator Manipulator { get { return manipulator; } }

            public double Accurasy
            {
                get { return accurasy; }
            }

            private void setParametres()
            {
                clearDisplayedContent();
                int i = 0;
                foreach (Link link in manipulator.Links)
                {
                    listBoxNumber.Items.Add(i);
                    listBoxLength.Items.Add(link.Length);
                    listBoxTheta.Items.Add(String.Format("{0:0.00}", link.Theta));
                    listBoxMinTheta.Items.Add(link.MinTheta);
                    listBoxMaxTheta.Items.Add(link.MaxTheta);
                    i++;
                }
                textBoxAccurasy.Text = accurasy.ToString();
            }

            private void clearDisplayedContent()
            {
                listBoxNumber.Items.Clear();
                listBoxLength.Items.Clear();
                listBoxTheta.Items.Clear();
                listBoxMinTheta.Items.Clear();
                listBoxMaxTheta.Items.Clear();
            }

            /// <summary>
            /// добавить звено к манипулятору
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            private void buttonAdd_Click(object sender, RoutedEventArgs e)
            {
                try
                {
                    bool parsed;
                    int number;
                    parsed = Int32.TryParse(textBoxNumber.Text, out number);
                    if (!parsed)
                        number = manipulator.Links.Count;
                    if (number < 0 || number > manipulator.Links.Count)
                        throw new ParametersException("Номер звена должен находиться в пределах [0," + (manipulator.Links.Count) + "].");

                    double length, theta, minTheta, maxTheta;
                    length = Double.Parse(textBoxLength.Text);
                    theta = Double.Parse(textBoxTheta.Text);
                    minTheta = Double.Parse(textBoxMinTheta.Text);
                    maxTheta = Double.Parse(textBoxMaxTheta.Text);

                    if (length > 100)
                        throw new ParametersException("Слишком большая длина звена. Введите число меньше 100.");

                    //if (minTheta > maxTheta)
                    //    throw new ParametersException("Максимальный угол должен быть больше минимального.");
                    double roratingAngle = manipulator.Links[0].RotatingAngle;
                    manipulator += new Link(length, theta, minTheta, maxTheta);
                    manipulator.AddRotatingAngle(roratingAngle);
                    manipulator.Rotate();
                //    clearFieldsForAdding();
                    setParametres();
                }
                catch (FormatException ex)
                {
                    MessageBox.Show("Все поля должны быть заполнены.\n" +
                        "Вещественные числа должны иметь вид #...,##...", "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                catch (ArgumentException ex)
                {
                    MessageBox.Show(ex.Message, "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                catch (ParametersException ex)
                {
                    MessageBox.Show(ex.Message, "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }


            private void clearFieldsForAdding()
            {
                textBoxNumber.Clear();
                textBoxLength.Clear();
                textBoxMinTheta.Clear();
                textBoxMaxTheta.Clear();
                textBoxTheta.Clear();
            }

            private void buttonRemove_Click(object sender, RoutedEventArgs e)
            {
                int linkNumber;
                try
                {
                    if (manipulator.Links.Count == 1)
                        MessageBox.Show("Манипулятор должен содержать хотя бы 1 звено.", "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);

                    linkNumber = Int32.Parse(textBoxForRemove.Text);

                    if (linkNumber < 0 || linkNumber >= manipulator.Links.Count)
                        throw new ParametersException("Номер звена должен находиться в пределах [0," + (manipulator.Links.Count - 1) + "].");

                    manipulator.RemoveAt(linkNumber);

                    textBoxForRemove.Clear();
                    setParametres();
                }
                catch (FormatException ex)
                {
                    MessageBox.Show("Нормер звена должен быть положительным целым числом.", "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                catch (ParametersException ex)
                {
                    MessageBox.Show(ex.Message, "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            
            private void buttonChange_Click(object sender, RoutedEventArgs e)
            {
                try
                {
                    bool parsed;
                    int number;
                    parsed = Int32.TryParse(textBoxNumber.Text, out number);
                    if (!parsed)
                        number = manipulator.Links.Count;
                    if (number < 0 || number > manipulator.Links.Count - 1)
                        throw new ParametersException("Номер звена должен находиться в пределах [0," + (manipulator.Links.Count - 1) + "].");

                    double length, theta, minTheta, maxTheta;
                    length = Double.Parse(textBoxLength.Text);
                    theta = Double.Parse(textBoxTheta.Text);
                    minTheta = Double.Parse(textBoxMinTheta.Text);
                    maxTheta = Double.Parse(textBoxMaxTheta.Text);

                    if (length > 100)
                        throw new ParametersException("Слишком большая длина звена. Введите число меньше 100.");
                    
                    manipulator.ChangeAt(number, new Link(length, theta, minTheta, maxTheta));
                    clearFieldsForAdding();
                    setParametres();
                }
                catch (FormatException ex)
                {
                    MessageBox.Show("Все поля должны быть заполнены.\n" +
                        "Вещественные числа должны иметь вид #...,##...", "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                catch (ArgumentException ex)
                {
                    MessageBox.Show(ex.Message, "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                catch (ParametersException ex)
                {
                    MessageBox.Show(ex.Message, "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }

            private void buttonChangeAccuracy_Click(object sender, RoutedEventArgs e)
            {
                try {
                    accurasy = Double.Parse(textBoxAccurasy.Text);
                }
                catch (FormatException ex)
                {
                    MessageBox.Show("Все поля должны быть заполнены.\n" +
                        "Вещественные числа должны иметь вид #...,##...", "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }
    }
}