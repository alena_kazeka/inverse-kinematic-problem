﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.IO.Ports;

namespace RoboticArmWpf.RoboArm
{
    /// <summary>
    /// Interaction logic for RoboArmWindow.xaml
    /// </summary>
    public partial class RoboArmWindow : Window
    {
        private SerialPort serialPort;
        private RoboArm roboArm;

        public RoboArmWindow(SerialPort serialPort)
        {
            InitializeComponent();

            roboArm = new RoboArm(degrees());

            try {
                this.serialPort = serialPort;
                serialPort.Open();

                setCommand();
            }
            catch(Exception ex)
            {
                MessageBox.Show("Порт не открывается!");
            }
        }

        private void sliderServo1_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            double value = sliderServo1.Value;
            tbServo1.Text = ((int)value).ToString();

            setCommand();
        }

        private void tbServo1_TextChanged(object sender, TextChangedEventArgs e)
        {
            //try
            //{
            //    double value = Double.Parse(tbServo1.Text);
            //    if (value < 500)
            //        tbServo1.Text = "500";
            //    else if (value > 2500)
            //        tbServo1.Text = "2500";

            //    sliderServo1.Value = value;
            //}
            //catch(Exception ex)
            //{
            //    MessageBox.Show("Введите число!", "Ошибка ввода",MessageBoxButton.OK,MessageBoxImage.Warning);
            //}
        }

        private void sliderServo2_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            double value = sliderServo2.Value;
            tbServo2.Text = ((int)value).ToString();

            setCommand();
        }

        private void sliderServo3_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            double value = sliderServo3.Value;
            tbServo3.Text = ((int)value).ToString();

            setCommand();
        }

        private void sliderServo4_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            double value = sliderServo4.Value;
            tbServo4.Text = ((int)value).ToString();

            setCommand();
        }

        private void sliderServo5_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            double value = sliderServo5.Value;
            tbServo5.Text = ((int)value).ToString();

            setCommand();
        }

        private void sliderServo6_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            double value = sliderServo6.Value;
            tbServo6.Text = ((int)value).ToString();

            setCommand();
        }

        private void setCommand()
        {
            if (this.IsLoaded)
            {
                int speed = int.Parse(tbSpeed.Text);
                int timeInterval = int.Parse(tbTimeInterval.Text);

                string command = roboArm.GetCommand(servoNumbers(), degrees(), speed, timeInterval);
                tbCommandBox.Text = command;
                
                if (serialPort.IsOpen)
                    serialPort.Write(convertUtfToASCII(command), 0, command.Length + 1);
            }
        }

        private byte[] convertUtfToASCII(string command)
        {
            command += '\r';
            
            // Create two different encodings.
            Encoding ascii = Encoding.ASCII;
            Encoding unicode = Encoding.Unicode;

            // Convert the string into a byte[].
            byte[] unicodeBytes = unicode.GetBytes(command);

            // Perform the conversion from one encoding to the other.
            byte[] asciiBytes = Encoding.Convert(unicode, ascii, unicodeBytes);

            return asciiBytes;
        }

        /// <summary>
        /// Отправить команду
        /// </summary>
        /// <param name="thetas">Нулевой элемент - угол вращения, остальные - углы наклонов звеньев снизу вверх</param>
        public void SendToRoboArm(List<double> thetas)
        {
            if (thetas.Count != 4)
                return;

            List<int> degrees = roboArm.FormatThetas(thetas, getDirectionForFirstFourServo());

            sliderServo1.ValueChanged -= sliderServo1_ValueChanged;
            sliderServo2.ValueChanged -= sliderServo2_ValueChanged;
            sliderServo3.ValueChanged -= sliderServo3_ValueChanged;
            sliderServo4.ValueChanged -= sliderServo4_ValueChanged;

            sliderServo1.Value = degrees[0];
            tbServo1.Text = ((int)degrees[0]).ToString();

            sliderServo2.Value = degrees[1];
            tbServo2.Text = ((int)degrees[1]).ToString();

            sliderServo3.Value = degrees[2];
            tbServo3.Text = ((int)degrees[2]).ToString();

            sliderServo4.Value = degrees[3];
            tbServo4.Text = ((int)degrees[3]).ToString();
            
            setCommand();
            
            sliderServo1.ValueChanged += sliderServo1_ValueChanged;
            sliderServo2.ValueChanged += sliderServo2_ValueChanged;
            sliderServo3.ValueChanged += sliderServo3_ValueChanged;
            sliderServo4.ValueChanged += sliderServo4_ValueChanged;
        }

        private List<bool> getDirectionForFirstFourServo()
        {
            List<bool> directions = new List<bool>();
            directions.Add(cbLeftServo1.IsChecked.Value);
            directions.Add(cbLeftServo2.IsChecked.Value);
            directions.Add(cbLeftServo3.IsChecked.Value);
            directions.Add(cbLeftServo4.IsChecked.Value);

            return directions;
        }


        private void bttnDisconnect_Click(object sender, RoutedEventArgs e)
        {
            serialPort.Close();
            this.Close();
        }

        private void setVisibilityOfElementsForChangeState(bool visible)
        {
            if (!visible)
            {
                lblSpeed.Visibility = Visibility.Visible;
                lblTimeInterval.Visibility = Visibility.Visible;
                tbSpeed.Visibility = Visibility.Visible;
                tbTimeInterval.Visibility = Visibility.Visible;

                tbCommandBox.IsEnabled = true;
                bttnDisconnect.IsEnabled = true;

                bttnSaveInitState.Visibility = Visibility.Collapsed;
            }
            else
            {
                lblSpeed.Visibility = Visibility.Collapsed;
                lblTimeInterval.Visibility = Visibility.Collapsed;
                tbSpeed.Visibility = Visibility.Collapsed;
                tbTimeInterval.Visibility = Visibility.Collapsed;

                tbCommandBox.IsEnabled = false;
                bttnDisconnect.IsEnabled = false;

                bttnSaveInitState.Visibility = Visibility.Visible;
            }
        }

        private void MenuItemSettings_Click(object sender, RoutedEventArgs e)
        {
            setVisibilityOfElementsForChangeState(true);
        }

        private List<int> servoNumbers()
        {
            List<int> servoNumbers = new List<int>();
            servoNumbers.Add(int.Parse(numberServo1.Text));
            servoNumbers.Add(int.Parse(numberServo2.Text));
            servoNumbers.Add(int.Parse(numberServo3.Text));
            servoNumbers.Add(int.Parse(numberServo4.Text));
            servoNumbers.Add(int.Parse(numberServo5.Text));
            servoNumbers.Add(int.Parse(numberServo6.Text));

            return servoNumbers;
        }

        private List<int> degrees()
        {
            List<int> degrees = new List<int>();
            degrees.Add((int)sliderServo1.Value);
            degrees.Add((int)sliderServo2.Value);
            degrees.Add((int)sliderServo3.Value);
            degrees.Add((int)sliderServo4.Value);
            degrees.Add((int)sliderServo5.Value);
            degrees.Add((int)sliderServo6.Value);

            return degrees;
        }
        
        private void bttnSaveInitState_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                List<int> initialState = degrees();
                roboArm = new RoboArm(initialState);

                setVisibilityOfElementsForChangeState(false);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Некорректный ввод номеров серво.", "Ошибка", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        public void MoveRoboArm(List<List<double>> findedThetas)
        {
            foreach (List<double> thetas in findedThetas)
            {
                SendToRoboArm(thetas);
                System.Threading.Thread.Sleep(500);
            }
        }
    }
}
