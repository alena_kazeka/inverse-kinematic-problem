﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RoboticArmWpf.RoboArm
{
    public class RoboArm
    {
        List<int> initialState;

        /// <summary>
        /// Конструктор класса RoboArm
        /// </summary>
        /// <param name="servoNumbers">Номера серво</param>
        /// <param name="initialState">Начальные состояния для каждого серво (90 градусов)</param>
        public RoboArm(List<int> initialState)
        {
            this.initialState = initialState;
        }

        public string GetCommand(List<int> servoNumbers, List<int> degrees, int speed, int timeInterval/*, List<int> direction*/)
        {
            StringBuilder command = new StringBuilder();
            //  int degree;
            for (int i = 0; i < servoNumbers.Count; i++)
            {
                //  degree = (direction[i] == -1 ? 180 - degrees[i] : degrees[i]);
                string part = string.Format("#{0}P{1}S{2}", servoNumbers[i], degrees[i], /*timeInterval,*/ speed);
                command.Append(part);
            }

            return command.ToString();
        }

        public List<int> FormatThetas(List<double> thetas, List<bool> direction)
        {
            List<int> degrees = new List<int>();

            int leftDirectionSetter;

            //вращение
            double multiplier;

            int reverseLinks = 180 * ((int)thetas[0] / 180);

            thetas[0] = thetas[0] % 180;

            if (thetas[0] <= 90)
            {
                multiplier = (initialState[0] - 500) / 90.0;
                degrees.Add((int)(multiplier * thetas[0]) + 500);
            }
            else
            {
                multiplier = (2500 - initialState[1]) / 90.0;
                degrees.Add(((int)(multiplier * thetas[0]) % 90) + 1500);
            }

            //для первого звена
            leftDirectionSetter = (direction[1] == false ? 0 : 180);
            double theta = Math.Abs(reverseLinks - Math.Abs(thetas[1] - leftDirectionSetter));
            if (thetas[1] <= 90)
            {
                multiplier = (initialState[1] - 500) / 90.0;
                double c = multiplier * ((90 - theta) % 90);
                degrees.Add(initialState[1] - (int)c);
            }
            else
            {
                multiplier = (2500 - initialState[1]) / 90.0;
                degrees.Add((int)(multiplier * theta) % 90 + initialState[1]);
            }

            //для второго и третьего
            for (int i = 2; i <= 3; i++)
            {
                leftDirectionSetter = (direction[i] == false ? 0 : 180);

                theta = (thetas[i] + 90) == 180 ? 180 : (thetas[i] + 90) % 180;

                theta = Math.Abs(reverseLinks - Math.Abs(theta - leftDirectionSetter));

                if (theta <= 90)
                {
                    multiplier = (initialState[i] - 500) / 90.0;
                    degrees.Add(initialState[i] - (int)(multiplier * ((90 - theta) % 90)));
                }
                else
                {
                    multiplier = (2500 - initialState[i]) / 90.0;
                    degrees.Add(initialState[i] + (int)(multiplier * (theta % 90)));
                }
            }

            return degrees;
        }
    }
}
