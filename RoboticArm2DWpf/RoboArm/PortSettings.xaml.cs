﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.IO.Ports;

namespace RoboticArmWpf.RoboArm
{
    /// <summary>
    /// Interaction logic for PortSettings.xaml
    /// </summary>
    public partial class PortSettings : Window
    {
        public PortSettings()
        {
            InitializeComponent();

            setBitsPerSecondComboBox();
            setDataBitsComboBox();
            setParityComboBox();
            setStopBitsComboBox();
            setFlowControlComboBox();

            setDictionary();
        }

        private void setBitsPerSecondComboBox()
        {
            List<String> lst = new List<String>(new String[] {
                "75","110","134","150","300","600","1200","1800","2400","4800","7200",
                "9600","14400","19200","38400","57600","115200","128000"
            });
            cbBitsPerSecond.ItemsSource = lst;
            cbBitsPerSecond.SelectedIndex = 16;
        }

        private void setDataBitsComboBox()
        {
            List<String> lst = new List<String>(new String[] {
                "4","5","6","7","8"
            });
            cbDataBits.ItemsSource = lst;
            cbDataBits.SelectedIndex = 4;
        }

        private void setParityComboBox()
        {
            List<String> lst = new List<String>(new String[] {
                "Чет","Нечет","Нет","Маркер","Пробел"
            });
            cbParity.ItemsSource = lst;
            cbParity.SelectedIndex = 2;
        }

        private void setStopBitsComboBox()
        {
            List<String> lst = new List<String>(new String[] {
                "1","1,5","2"
            });
            cbStopBits.ItemsSource = lst;
            cbStopBits.SelectedIndex = 0;
        }

        private void setFlowControlComboBox()
        {
            List<String> lst = new List<String>(new String[] {
                "Xon/Xoff","Аппаратное","Нет","Аппаратное+Xon/Xoff"
            });
            cbFlowControl.ItemsSource = lst;
            cbFlowControl.SelectedIndex = 2;
        }

        //словарь для трансляции русского на английский
        Dictionary<String, String> ruToEn;

        void setDictionary()
        {
            ruToEn = new Dictionary<string, string>();
            ruToEn.Add("Нет", "None");
            ruToEn.Add("Чет", "Even");
            ruToEn.Add("Нечет", "Odd");
            ruToEn.Add("Маркер", "Mark");
            ruToEn.Add("Пробел", "Space");
            ruToEn.Add("Аппаратное", "RequestToSend");
            ruToEn.Add("Xon/Xoff", "XOnXOff");
            ruToEn.Add("Аппаратное+Xon/Xoff", "RequestToSendXOnXOff");
        }

        public SerialPort serialPort { get; private set; }

        private void bttnConnect_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                String portName = tbPort.Text;
                
                //сделать проверку имени порта с помощью регулярных выражений
                // regex @"COM[0-9]+" 
                //выбросить исключение, если подстрока-совпадение не равна имени порта


                int bitsPerSecond = Int32.Parse(cbBitsPerSecond.SelectedValue.ToString());
                int dataBits = Int32.Parse(cbDataBits.SelectedValue.ToString());
                String parity = cbParity.SelectedValue.ToString();
                double stopBits = Double.Parse(cbStopBits.SelectedValue.ToString());
                String flowControl = cbFlowControl.SelectedValue.ToString();

                Parity prt = (Parity)Enum.Parse(typeof(Parity), ruToEn[parity]);

                StopBits st;
                if (stopBits.CompareTo(1) == 0) st = StopBits.One;
                else if (stopBits.CompareTo(1.5) == 0) st = StopBits.OnePointFive;
                else st = StopBits.Two;

                serialPort = new SerialPort(portName,bitsPerSecond,prt,dataBits,st);
                serialPort.Handshake = (Handshake)Enum.Parse(typeof(Handshake), ruToEn[flowControl]);

                serialPort.WriteTimeout = 500;

                this.Close();
            }
            catch (Exception ex)
            {

            }
        }

    }
}
