﻿using System.Collections.Generic;
using OxyPlot;
using OxyPlot.Axes;
using OxyPlot.Series;

namespace RoboticArmWpf.Plot
{
    public class DynamicPlotModel
    {
        private PlotModel plotModel;
        public PlotModel PlotModel
        {
            get { return plotModel; }
            set { plotModel = value; }
        }
        
        public DynamicPlotModel(List<double> thetas, int linkNnumber)
        {
            PlotModel = new PlotModel();
            SetUpModel(linkNnumber);
            LoadData(thetas);
        }

        private readonly OxyColor color = OxyColors.DarkCyan;

        private readonly MarkerType markerType = MarkerType.Triangle;

        private void SetUpModel(int linkNumber)
        {
            PlotModel.IsLegendVisible = false;

            var valueAxis = new LinearAxis
            {
                Position = AxisPosition.Left,
                Minimum = 0,
                MajorGridlineStyle = LineStyle.Solid,
                MinorGridlineStyle = LineStyle.Dot,
                Title = linkNumber + ": угол наклона"
            };
            var iterAxis = new LinearAxis
            {
                Position = AxisPosition.Bottom,
                Minimum = 0,
                Title = "Итерация",
                MajorGridlineStyle = LineStyle.Solid,
                MinorGridlineStyle = LineStyle.None
            };
            PlotModel.Axes.Add(iterAxis);
            PlotModel.Axes.Add(valueAxis);
        }

        private void LoadData(List<double> thetas)
        {
            var lineSerie = new LineSeries
            {
                StrokeThickness = 2,
                MarkerSize = 3,
                MarkerStroke = OxyColors.DarkGray,
                MarkerType = markerType,
                CanTrackerInterpolatePoints = true,
                Smooth = true
            };

            lineSerie.Color = color;

            int i = 0;
            thetas.ForEach(t => lineSerie.Points.Add(new DataPoint(i++, t)));

            PlotModel.Series.Add(lineSerie);
        }
    }
}
