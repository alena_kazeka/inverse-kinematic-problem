﻿using System.Collections.Generic;
using OxyPlot;
using OxyPlot.Axes;
using OxyPlot.Series;

namespace RoboticArmWpf.Plot
{
    public class ErrorPlotModel
    {
        private PlotModel plotModel;
        public PlotModel PlotModel
        {
            get { return plotModel; }
            set { plotModel = value;}
        }

        public ErrorPlotModel(List<double> errors)
        {
            PlotModel = new PlotModel();
            SetUpModel();
            LoadData(errors);
        }

        private readonly OxyColor color = OxyColors.DarkBlue;

        private readonly MarkerType markerType = MarkerType.Triangle;

        private void SetUpModel()
        {
            PlotModel.IsLegendVisible = false;

            var valueAxis = new LinearAxis
            {
                Position = AxisPosition.Left,
                Minimum = 0, MajorGridlineStyle = LineStyle.Solid,
                MinorGridlineStyle = LineStyle.Dot,
                Title = "Ошибка, см"
            };
            var iterAxis = new LinearAxis
            {
                Position = AxisPosition.Bottom,
                Title = "Итерация",
                MajorGridlineStyle = LineStyle.Solid,
                MinorGridlineStyle = LineStyle.None
            };
            PlotModel.Axes.Add(iterAxis);
            PlotModel.Axes.Add(valueAxis);
        }

        private void LoadData(List<double> errors)
        {
            var lineSerie = new LineSeries
            {
                StrokeThickness = 2,
                //MarkerSize = 3,
                //MarkerStroke = OxyColors.DarkGray,
                //MarkerType = markerType,
                CanTrackerInterpolatePoints = false,
                Smooth = false
            };

            lineSerie.Color = color;

            int i = 0;
            errors.ForEach(er => lineSerie.Points.Add(new DataPoint(i++, er)));

            PlotModel.Series.Add(lineSerie);
        }
    }
}
