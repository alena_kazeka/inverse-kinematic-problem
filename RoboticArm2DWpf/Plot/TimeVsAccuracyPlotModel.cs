﻿using System.Collections.Generic;
using OxyPlot;
using OxyPlot.Axes;
using OxyPlot.Series;

namespace RoboticArmWpf.Plot
{
    public class TimeVsAccuracyPlotModel
    {
        private PlotModel plotModel;
        public PlotModel PlotModel
        {
            get { return plotModel; }
            set { plotModel = value; }
        }

        public TimeVsAccuracyPlotModel(Dictionary<double, double> accuracyTime)
        {
            PlotModel = new PlotModel();
            setUpModel();
            loadData(accuracyTime);
        }

        private readonly OxyColor color = OxyColors.DarkBlue;

        private readonly MarkerType markerType = MarkerType.Diamond;

        private void setUpModel()
        {
            PlotModel.IsLegendVisible = false;

            var valueAxis = new LinearAxis
            {
                Position = AxisPosition.Left,
                Minimum = 0,
                MajorGridlineStyle = LineStyle.Solid,
                MinorGridlineStyle = LineStyle.Dot,
                Title = "Время, с"
            };
            var iterAxis = new LinearAxis
            {
                Position = AxisPosition.Bottom,
                Minimum = 0,
                Title = "Точность, x10e-3 мм",
                MajorGridlineStyle = LineStyle.Solid,
                MinorGridlineStyle = LineStyle.None                
            };
            PlotModel.Axes.Add(iterAxis);
            PlotModel.Axes.Add(valueAxis);
        }

        private void loadData(Dictionary<double, double> accuracyTime)
        {
            var lineSerie = new LineSeries
            {
                StrokeThickness = 2,
                MarkerSize = 3,
                MarkerStroke = OxyColors.OrangeRed,
                MarkerType = markerType,
                CanTrackerInterpolatePoints = false,
                Smooth = false
            };

            lineSerie.Color = color;

            foreach (KeyValuePair<double, double> pair in accuracyTime)
            {
                lineSerie.Points.Add(new DataPoint(pair.Key, pair.Value));
            }
            PlotModel.Series.Add(lineSerie);
        }
    }
}
