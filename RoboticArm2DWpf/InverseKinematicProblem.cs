﻿using System.Collections.Generic;
using System;
using DataConverterLib;
using GeneticAlgorithmsLib.HillClimbingGALib;
using GeneticAlgorithmsLib;
using RoboticArmLib;
using System.Windows.Media.Media3D;
using System.Diagnostics;
using StatisticsGALib;

namespace RoboticArmWpf
{
    public enum AlgorithmGA
    {
        HillClimbing,
        Standart
    }

    /// <summary>
    /// Класс для решения обратной кинематической задачи с помощью генетических алгоритмов
    /// </summary>
    public class InverseKinematicProblem
    {
        //генетические алгоритмы
        HillClimbingGA hillClimbing;
        StandartGA standartGA;

        //манипулятор для решения обратной задачи
        HandManipulator manipulator;

        public InverseKinematicProblem(HandManipulator manipulator)
        {
            this.manipulator = manipulator;
        }

        //желаемая конечная точка
        Point3D endPoint;

        public Point3D EndPoint
        {
            get { return endPoint; }
            set { endPoint = value; }
        }

        //угол поворота манипулятора вокруг своей оси
        public double Phi
        {
            get;
            private set;
        }

        //поворот точки на заданный угол (перенос)
        private Point3D rotateEndPoint(Point3D point, bool forward = true)
        {
            if (forward)
            {
                if (Phi == 90)
                    point.X = point.Z;
                else
                    point.X = point.X * Math.Cos(Phi * Math.PI / 180) + point.Z * Math.Sin(Phi * Math.PI / 180);
                point.Z = 0;
                return point;
            }

            Point3D copy = point;
            point.X = copy.X * Math.Cos(-Phi * Math.PI / 180);
            point.Z = -copy.X * Math.Sin(-Phi * Math.PI / 180);

            return point;
        }

        /// <summary>
        /// получить пригодность особи для генетического алгоритма
        /// </summary>
        /// <param name="genes">особь, представленная в виде массива символов</param>
        /// <returns>пригодность - евклидово расстояние до конечной точки</returns>
        private double fitness(char[] genes)
        {
            List<double> thetas = ConveterForGA.CharArrayToThetasList(genes);
            if (!manipulator.ChangeThetas(thetas))
                return 1000.0;
            Vector3D vector = new Vector3D(manipulator.EndPoint2D.X - endPoint.X, manipulator.EndPoint2D.Y - endPoint.Y, manipulator.EndPoint2D.Z - endPoint.Z);
            return vector.Length;
        }

        /// <summary>
        /// Получить пригодность особи для генетического алгоритма
        /// </summary>
        /// <param name="genes">особь, представленная в виде массива вещественных чисел</param>
        /// <returns>пригодность - евклидово расстояние до конечной точки</returns>
        private double fitness(double[] genes)
        {
            manipulator.ChangeThetas(new List<double>(genes));
            Vector3D vector = new Vector3D(manipulator.EndPoint2D.X - endPoint.X, manipulator.EndPoint2D.Y - endPoint.Y, manipulator.EndPoint2D.Z - endPoint.Z);
            return vector.Length;
        }


        /// <summary>
        /// Найти углы поворота звеньев манипулятора по заданной конечной точке с помощью заданного алгоритма и с заданной точностью.
        /// Используется в тестировании.
        /// </summary>
        /// <param name="point">желаемая конечная точка схвата манипулятора</param>
        /// <param name="algorithm">выбранный генетический алгоритм для решения обратной кинематической задачи</param>
        /// <param name="accuracy">точность вычислений (евклидово расстояние от заданной точки до полученной при решении обратной задачи)</param>
        /// <returns></returns>
        public double[] FindThetas(Point3D point, AlgorithmGA algorithm, double accuracy)
        {
            //найти угол поворота манипулятора для проекции его в двумерную плоскость
            setPhi(point);

            //спроецировать конечную точку в двумерную плоскость
            endPoint = rotateEndPoint(point);

            //решение обратной кинематической задачи в зависимости от выбранного алгоритма
            if (algorithm == AlgorithmGA.HillClimbing)
            {
                Chromosome chromosome = new Chromosome(ConveterForGA.ThetasListToCharArray(manipulator.Thetas), fitness);
                hillClimbing = new HillClimbingGA(chromosome);
                char[] thetas = hillClimbing.Evolve(0.05, accuracy);

                double[] result = ConveterForGA.CharArrayToThetasList(thetas).ToArray();

                return result;
            }
            else if (algorithm == AlgorithmGA.Standart)
            {
                double[][] restriction = new double[2][];
                restriction[0] = new double[manipulator.Links.Count];
                restriction[1] = new double[manipulator.Links.Count];
                for (int i = 0; i < manipulator.Links.Count; i++)
                {
                    restriction[0][i] = manipulator.Links[i].MinTheta;
                    restriction[1][i] = manipulator.Links[i].MaxTheta;
                }

                standartGA = new StandartGA(100, manipulator.Links.Count, restriction, fitness);

                double[] result = standartGA.Evolve(accuracy);

                return result;
            }

            return null;
        }

        /// <summary>
        /// Найти углы поворота звеньев манипулятора по заданной конечной точке с помощью заданного алгоритма и с заданной точностью.
        /// Используется при непосредственной работе с симулятором для отображения найденного результата в виде пространственного положения манипулятора.
        /// </summary>
        /// <param name="point">желаемая конечная точка схвата манипулятора</param>
        /// <param name="algorithm">выбранный генетический алгоритм для решения обратной кинематической задачи</param>
        /// <param name="accuracy">точность вычислений (евклидово расстояние от заданной точки до полученной при решении обратной задачи)</param>
        /// <returns>Список найденных в процессе решения генетическим алгоритмом точек, последняя из которых является решением</returns>
        public List<Point3D> Solve(Point3D point, AlgorithmGA algorithm, double accuracy)
        {
            setPhi(point);
            endPoint = rotateEndPoint(point);

            List<Point3D> endPoints = new List<Point3D>();
            if (algorithm == AlgorithmGA.HillClimbing)
            {
                Chromosome chromosome = new Chromosome(ConveterForGA.ThetasListToCharArray(manipulator.Thetas), fitness);
                hillClimbing = new HillClimbingGA(chromosome);
                hillClimbing.Evolve(0.1, accuracy);

                List<char[]> thetasGenerations = hillClimbing.Generations;
                foreach (char[] genes in thetasGenerations)
                {
                    manipulator.ChangeThetas(ConveterForGA.CharArrayToThetasList(genes));
                    endPoints.Add(manipulator.EndPoint3D);//RotateEndPoint(manipulator.EndPoint2D, false));
                }
            }
            else if (algorithm == AlgorithmGA.Standart)
           {
                double[][] restriction = manipulator.GetRestrictions();

                standartGA = new StandartGA(100, manipulator.Links.Count, restriction, fitness);
                standartGA.Evolve(accuracy);

                List<double[]> thetasGenerations = standartGA.Generations;

                foreach (double[] genes in thetasGenerations)
                {
                    manipulator.ChangeThetas(new List<double>(genes));
                    endPoints.Add(manipulator.EndPoint3D); 
                    //endPoints.Add(RotateEndPoint(manipulator.EndPoint2D, false));
                }
            }
            manipulator.Rotate();
            return endPoints;
        }

        ///// <summary>
        ///// Словарь для хранения решенний ОКЗ
        ///// </summary>
        //private Dictionary<Point3D, double[]> solvedPoints = new Dictionary<Point3D, double[]>();
        ///// <summary>
        ///// Максимальный размер solvedPoints
        ///// </summary>
        //private const int MAX_DICTIONARY_COUNT = 500;

        private Random rand = new Random(DateTime.Now.Millisecond);

        /// <summary>
        /// Получить пригодность особи для генетического алгоритма
        /// </summary>
        /// <param name="genes">особь, представленная в виде массива вещественных чисел</param>
        /// <returns>пригодность - евклидово расстояние до конечной точки</returns>
        private double dynamicFitness(double[] genes)
        {
            manipulator.ChangeThetas(new List<double>(genes));
            Vector3D vector = new Vector3D(manipulator.EndPoint2D.X - endPoint.X, manipulator.EndPoint2D.Y - endPoint.Y, manipulator.EndPoint2D.Z - endPoint.Z);
            //сохранить решение для точки
            //if (rand.NextDouble() < 0.05)
            //{
            //    Point3D key = changePointPrecision(manipulator.EndPoint3D, 2);
            //    if (!solvedPoints.ContainsKey(key))
            //        solvedPoints.Add(key, (double[])genes.Clone());
            //}
            return vector.Length;
        }

        //private Point3D changePointPrecision(Point3D point, int precision)
        //{
        //    return new Point3D(Math.Round(point.X, precision),
        //        Math.Round(point.Y, precision),
        //        Math.Round(point.Z));
        //}


        /// <summary>
        /// движение по заданной траектории
        /// </summary>
        /// <param name="LineToPoints">Траектория, разбитая на точки</param>
        /// <param name="accuracy">Точность вычислений</param>
        /// <returns>Список углов наклона звеньев манипулятора - решения ОКЗ - для каждой заданной точки + 0 элемент - угол вращения</returns>
        public List<List<double>> SolveDynamic(List<Point3D> lineToPoints, double accuracy)
        {
            List<List<double>> findedThetas = new List<List<double>>();

            //формируем массив ограничений на углы поворота звеньев
            double[][] restrictions = manipulator.GetRestrictions();
            double[][] newRestrictions = manipulator.GetRestrictions();

          //  int callsToDictionary = 0;
            List<double> result;
            StandartGaDynamic stGaDynamic;

            //сохраняем информацию о четвертях, в которых находятся углы
            List<int[]> quarters = new List<int[]>();

            Stopwatch stopwatch = new Stopwatch();
            Statistics.ErrorsGA.Clear();
            stopwatch.Start();
            //для каждой точки из списка
            foreach (Point3D point in lineToPoints)
            {
                //найти угол поворота оси
                setPhi(point);

                endPoint = rotateEndPoint(point);

                //  Point3D key = changePointPrecision(point, 2);
                //if (solvedPoints.ContainsKey(key))
                //{
                ////взять сохраненное решения для данной точки
                //result = (new List<double>((double[])solvedPoints[key].Clone()));
                //callsToDictionary++;

                //newRestrictions = addRestrictions(restrictions, solvedPoints[key]);
                //}
                //else
                //{
                //решить ОКЗ
                stGaDynamic = new StandartGaDynamic(100, manipulator.Links.Count, newRestrictions, dynamicFitness);
                double[] thetas = stGaDynamic.Evolve(accuracy);
                if (thetas == null)
                {
                    standartGA = new StandartGA(100, manipulator.Links.Count, restrictions, dynamicFitness);
                    thetas = standartGA.Evolve(accuracy);
                }
                result = new List<double>(thetas);
                newRestrictions = addRestrictions(restrictions, thetas);
                //   }

                int[] q = new int[result.Count];
                for (int i = 0; i < q.Length; i++)
                    q[i] = getQuarter(result[i]);
                quarters.Add(q);


                //0 элемент - угол вращение
                result.Insert(0, Phi);
                findedThetas.Add(result);

                manipulator.Rotate();

            }

            //--проанализировать quaters
            HashSet<int> indexesForRecalculation = analiseQuaters(quarters);
            if (indexesForRecalculation.Count > 0)
            {
                //находим точку, в которой не надо пересчитывать углы
                int indexForRestrictions = 0;
                for (int i = lineToPoints.Count - 1; i >= 0; i--)
                    if (!indexesForRecalculation.Contains(i))
                    {
                        indexForRestrictions = i;
                        break;
                    }

                //пересчитать
                double[] thetasForResctriction = findedThetas[indexForRestrictions].GetRange(1, manipulator.Links.Count).ToArray();
                newRestrictions = addRestrictions(restrictions, thetasForResctriction);
                foreach (int i in indexesForRecalculation)
                {
                    Point3D point = lineToPoints[i];
                    //найти угол поворота оси
                    setPhi(point);
                    endPoint = rotateEndPoint(point);

                    //решить ОКЗ
                    stGaDynamic = new StandartGaDynamic(100, manipulator.Links.Count, newRestrictions, dynamicFitness);
                    double[] thetas = stGaDynamic.Evolve(accuracy);
                    if (thetas == null)
                    {
                        standartGA = new StandartGA(100, manipulator.Links.Count, restrictions, dynamicFitness);
                        thetas = standartGA.Evolve(accuracy);
                    }
                    result = new List<double>(thetas);

                    //0 элемент - угол вращение
                    result.Insert(0, Phi);
                    findedThetas[i] = result;

                    manipulator.Rotate();
                }
            }

            quarters.Clear();
            foreach (List<double> r in findedThetas)
            {
                int[] q = new int[r.Count - 1];
                for (int i = 0; i < q.Length; i++)
                    q[i] = getQuarter(r[i+1]);
                quarters.Add(q);
            }

            //--
            stopwatch.Stop();
            Statistics.SaveDynamicStatistic(stopwatch.Elapsed.TotalMilliseconds, indexesForRecalculation.Count, analiseQuaters(quarters).Count);

            //if (solvedPoints.Count >= MAX_DICTIONARY_COUNT)
            //    solvedPoints.Clear();

            return findedThetas;
        }

        private void setPhi(Point3D point)
        {
            if (point.X == 0)
                Phi = 90;
            else
                Phi = Math.Atan(point.Z / point.X) * 180 / Math.PI;
            manipulator.AddRotatingAngle(Phi);
        }

        /// <summary>
        /// Анализ четвертей, в которых находятся углы наклона звеньев
        /// </summary>
        /// <param name="quarters">набор четвертей</param>
        /// <returns>список точек, которые надо пересчитать</returns>
        private HashSet<int> analiseQuaters(List<int[]> quarters)
        {
            //для каждого звена находим четверть, в которой оно чаще всего находится
            int[] maxFreq = new int[quarters[0].Length];
            int currentMaxValue;
            for (int i = 0; i < quarters[0].Length; i++)
            {
                int[] freq = new int[4];
                for (int j = 0; j < quarters.Count; j++)
                {
                   freq[quarters[j][i] - 1]++;
                }
                currentMaxValue = freq[0];
                maxFreq[i] = 1;
                for (int k = 1; k < freq.Length; k++)
                    if (currentMaxValue < freq[k])
                    {
                        currentMaxValue = freq[k];
                        maxFreq[i] = k + 1;
                    }
            }

            HashSet<int> indexesOfPointsToRecalculate = new HashSet<int>();
            for (int i = 0; i < quarters.Count; i++)
            {
                for (int j = 0; j < quarters[i].Length; j++)
                {
                    if (quarters[i][j] != maxFreq[j])
                        indexesOfPointsToRecalculate.Add(i);
                }
            }

            return indexesOfPointsToRecalculate;
        }

        private double[][] addRestrictions(double[][] resctictions, double[] currentThetas)
        {
            double[][] newRestrictions = new double[2][];
            for (int i = 0; i < 2; i++)
                newRestrictions[i] = new double[currentThetas.Length];

            for (int i = 0; i < currentThetas.Length; i++)
            {
                if (currentThetas[i] < 90)
                {
                    if (resctictions[1][i] >= 90)
                        newRestrictions[1][i] = 90;
                    else
                        newRestrictions[1][i] = resctictions[1][i];
                    newRestrictions[0][i] = 0;
                }
                else if (currentThetas[i] >= 90 && currentThetas[i] < 180)
                {
                    if (resctictions[0][i] > 90 && resctictions[0][i] < 180)
                        newRestrictions[0][i] = resctictions[0][i];
                    else
                        newRestrictions[0][i] = 90;
                    if (resctictions[1][i] >= 180)
                        newRestrictions[1][i] = 180;
                    else
                        newRestrictions[1][i] = resctictions[1][i];
                }
                else if (currentThetas[i] >= 180 && currentThetas[i] < 270)
                {
                    if (resctictions[0][i] > 180 && resctictions[0][i] < 270)
                        newRestrictions[0][i] = resctictions[0][i];
                    else
                        newRestrictions[0][i] = 180;
                    if (resctictions[1][i] >= 270)
                        newRestrictions[1][i] = 270;
                    else
                        newRestrictions[1][i] = resctictions[1][i];
                }
                else if (currentThetas[i] >= 270 && currentThetas[i] < 360)
                {
                    if (resctictions[0][i] > 270 && resctictions[0][i] < 360)
                        newRestrictions[0][i] = resctictions[0][i];
                    else
                        newRestrictions[0][i] = 270;
                    if (resctictions[1][i] >= 360)
                        newRestrictions[1][i] = 360;
                    else
                        newRestrictions[1][i] = resctictions[1][i];
                }
            }

            return newRestrictions;
        }

        private int getQuarter(double theta)
        {
            theta = theta % 360;
            if (theta >= 0 && theta < 90)
                return 1;
            if (theta >= 90 && theta < 180)
                return 2;
            if (theta >= 180 && theta < 270)
                return 3;
            if (theta >= 270 && theta < 360)
                return 4;

            return 0;
        }
    }
}
