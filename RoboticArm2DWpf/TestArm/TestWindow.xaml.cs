﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using RoboticArmLib;
using System.Windows.Media.Media3D;
using System.IO;
using System.Diagnostics;
using System.ComponentModel;
using RoboticArmWpf.ManipulatorParameters;

namespace RoboticArmWpf
{
    namespace TestArm
    {
        /// <summary>
        /// Interaction logic for TestWindow.xaml
        /// </summary>
        public partial class TestWindow : Window
        {
            public TestWindow(List<Link> links)
            {
                InitializeComponent();
                initializeManipulator(links);
            }

            private HandManipulator manipulator;

            private void initializeManipulator(List<Link> links)
            {
                manipulator = new HandManipulator(links);
            }

            private int testCount;
            private string testsInfoFilename = "./testInfo.txt";
            private string reportsFilename = "./reports.txt";

            private void btnStart_Click(object sender, RoutedEventArgs e)
            {
                try
                {
                    double x = Double.Parse(valueX.Text);
                    double y = Double.Parse(valueY.Text);
                    double z = Double.Parse(valueZ.Text);

                    testCount = Int32.Parse(testNumber.Text);
                    pbTests.Value = 0;
                    testsPassed.Content = "0/" + testCount;
                    testManipulator(testCount, new Point3D(x, y, z));
                }
                catch (FormatException ex)
                {
                    MessageBox.Show("Числа введены в неправильном формате.\n" +
                        "Вещественные числа должны иметь вид ##..,##..", "Ошибка", MessageBoxButton.OK);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }

            private void testManipulator(int testCount, Point3D endPoint)
            {
                try
                {
                    writer = new StreamWriter(testsInfoFilename); writer.Close();

                    BackgroundWorker worker = new BackgroundWorker();
                    worker.WorkerReportsProgress = true;
                    worker.DoWork += (o, ea) =>
                    {
                        Dispatcher.Invoke((Action)(() =>
                        {
                            lblIterCount.Content = 0;
                            lblTime.Content = 0 + " c";
                            lblIterTime.Content = 0 + " мс";
                            lblAvrgError.Content = 0 + " мм";
                            btnMoreInfo.IsEnabled = false;
                        }));
                        for (int i = 0; i < testCount; i++)
                        {
                            Dispatcher.Invoke((Action)(() =>
                            {
                                double[] thetas = makeTest(i, endPoint);
                                saveTestsInfo(i + 1, thetas);
                            }));
                            (o as BackgroundWorker).ReportProgress((i + 1) * 100 / testCount);
                        }
                    };
                    worker.ProgressChanged += (o, ea) =>
                    {
                        pbTests.Value = ea.ProgressPercentage;
                        testsPassed.Content = (ea.ProgressPercentage*testCount/100).ToString() + "/" + testCount;
                    };
                    worker.RunWorkerCompleted += (o, ea) =>
                    {
                        saveReport();
                        btnMoreInfo.IsEnabled = true;
                        displayTestsResult();
                    };

                    worker.RunWorkerAsync();
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.Message);
                }
            }

            private void displayTestsResult()
            {
                try
                {
                    reader = new StreamReader(testsInfoFilename);
                    string report = reader.ReadToEnd();
                    reader.Close();

                    string[] parsedReport = report.Split(' ', '\n', '\r', '\t');
                    List<double> results = new List<double>();
                    double dnum;
                    bool isNum;
                    foreach (string str in parsedReport)
                    {
                        if (!string.IsNullOrEmpty(str))
                        {
                            isNum = double.TryParse(str, out dnum);
                            if (isNum)
                                results.Add(dnum);
                        }
                    }

                    double iterationNumber = 0, totalTime = 0, timePerIteration = 0, error = 0;
                    int linksCount = manipulator.Links.Count;
                    if (results.Count % 4 != 0)
                        throw new ArgumentException();

                    for (int i = 0; i < results.Count; i++)
                    {
                        iterationNumber += results[i++];
                        totalTime += results[i++];
                        timePerIteration += results[i++];
                        error += results[i];
                    }

                    iterationNumber /= results.Count / 4;
                    totalTime /= results.Count / 4;
                    timePerIteration = iterationNumber == 0 ? 0 : (totalTime * 1000 / iterationNumber);
                    error /= results.Count / 4;

                    lblIterCount.Content = String.Format("{0:0.000}", iterationNumber);
                    lblTime.Content = String.Format("{0:0.00000} с", totalTime);
                    lblIterTime.Content = String.Format("{0:0.000} мс", timePerIteration);
                    lblAvrgError.Content = String.Format("{0:0.00} мм", error);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("В файле отчета о тестировании содержаться некоторые ошибки.\nНевозможно определить результат.", "Ошибка", MessageBoxButton.OK);
                }
            }


            /// <summary>
            /// тестирование руки-манипулятора
            /// </summary>
            /// <param name="testIndex">текущий номер теста</param>
            /// <param name="endPoint">желаемая конечная точка руки-манипулятора</param>
            private double[] makeTest(int testIndex, Point3D endPoint)
            {
                HandManipulator testManipulator = new HandManipulator(manipulator.LinksCopy);
                InverseKinematicProblem inversKinematicProblem = new InverseKinematicProblem(testManipulator);
                return inversKinematicProblem.FindThetas(endPoint, rbHillClimbing.IsChecked.Value
                    ? AlgorithmGA.HillClimbing : AlgorithmGA.Standart, accuracy);
            }

            private static StreamWriter writer;
            private static StreamReader reader;

            /// <summary>
            /// сохранение информации о прохождении теста
            /// </summary>
            /// <param name="testIndex">текущий номер теста</param>
            private void saveTestsInfo(int testIndex, double[] thetas)
            {
                String statistic = StatisticsGALib.Statistics.IterationsStatistics();
                writer = new StreamWriter(testsInfoFilename, true);
                writer.WriteLine("----Test " + testIndex + "----");
                StringBuilder thetasBuilder = new StringBuilder();
                foreach (double theta in thetas)
                    thetasBuilder.Append(string.Format("{0:0.00}", theta) + ";_");
                writer.WriteLine("thetas: " + thetasBuilder.ToString());
                writer.WriteLine(statistic);
                writer.WriteLine("");
                writer.Close();
            }

            /// <summary>
            /// сохранение информации о всех тестах
            /// </summary>
            private void saveReport()
            {
                writer = new StreamWriter(reportsFilename, true);
                reader = new StreamReader(testsInfoFilename);
                writer.WriteLine("########   " + DateTime.Now.Day + "." + DateTime.Now.Month + "." +
                    DateTime.Now.Year + "  " + DateTime.Now.Hour + ":" + DateTime.Now.Minute+ " ########");
                StringBuilder thetasBuilder = new StringBuilder();
                StringBuilder linksLength = new StringBuilder();
                StringBuilder thetasRestrictions = new StringBuilder();
                foreach (Link link in manipulator.Links)
                {
                    thetasBuilder.Append(string.Format("{0:0.00}", link.Theta) + " ");
                    linksLength.Append(link.Length + ";_");
                    thetasRestrictions.Append("[" + String.Format("{0:0.00}", link.MinTheta) + ";" + String.Format("{0:0.00}", link.MaxTheta) + "] ");
                }
                writer.WriteLine("");
                writer.WriteLine("initial thetas of arm: " + thetasBuilder.ToString());
                writer.WriteLine("restrictions of thetas: " + thetasRestrictions.ToString());
                writer.WriteLine("lenghts of links: " + linksLength.ToString());
                writer.WriteLine("algorithm: " + (rbHillClimbing.IsChecked.Value ? rbHillClimbing.Content.ToString() : rbStandart.Content.ToString()));
                writer.WriteLine("");
                writer.WriteLine(reader.ReadToEnd());
                writer.WriteLine("");
                reader.Close();
                writer.Close();
            }

            private void btnMoreInfo_Click(object sender, RoutedEventArgs e)
            {
                reader = new StreamReader(@".\testInfo.txt");
                String report = reader.ReadToEnd();
                reader.Close();

                TextWindow reportWindow = new TextWindow(report, "Отчет");
                reportWindow.Show();
            }

            private double accuracy = 0.1;

            private void btnManipulatorParameters_Click(object sender, RoutedEventArgs e)
            {
                ManipulatorParametersWindow parameters = new ManipulatorParametersWindow(manipulator.LinksCopy, accuracy);
                parameters.Closing += (o, ea) =>
                {
                    manipulator = parameters.Manipulator;
                    accuracy = parameters.Accurasy;                  
                };
                parameters.ShowDialog();
            }
        }
    }
}
