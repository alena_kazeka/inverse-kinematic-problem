﻿using RoboticArmLib;
using RoboticArmWpf.ManipulatorParameters;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Windows;
using System.Windows.Media.Media3D;

namespace RoboticArmWpf.TestArm
{
    /// <summary>
    /// Interaction logic for TestTimeAccuracy.xaml
    /// </summary>
    public partial class TestTimeAccuracy : Window
    {
        public TestTimeAccuracy(List<Link> links)
        {
            InitializeComponent();
            accuracyTime = new Dictionary<double, double>();
            initializeManipulator(links);

            accuracyTime.Add(0, 0);
            plotModel = new Plot.TimeVsAccuracyPlotModel(accuracyTime);
            plot.DataContext = plotModel;
        }

        private HandManipulator manipulator;
        private Plot.TimeVsAccuracyPlotModel plotModel;
        private void initializeManipulator(List<Link> links)
        {
            manipulator = new HandManipulator(links);
        }
        
        private void btnManipulatorParameters_Click(object sender, RoutedEventArgs e)
        {
            ManipulatorParametersWindow parameters = new ManipulatorParametersWindow(manipulator.LinksCopy);
            parameters.Closing += (o, ea) =>
            {
                manipulator = parameters.Manipulator;
            };
            parameters.ShowDialog();
        }

        private Point3D endPoint;

        private void btnStart_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                double x = Double.Parse(valueX.Text);
                double y = Double.Parse(valueY.Text);
                double z = Double.Parse(valueZ.Text);

                int testCount = Int32.Parse(testNumber.Text);
                pbTests.Value = 0;
                testsPassed.Content = "1%";
                endPoint = new Point3D(x, y, z);

                //double startAccuracy = Double.Parse(valueStartAccuracy.Text);
                //double endAccuracy = Double.Parse(valueEndAccuracy.Text);
                testManipulator(testCount,endPoint/*,startAccuracy, endAccuracy*/);
            }
            catch (FormatException ex)
            {
                MessageBox.Show("Числа введены в неправильном формате.\n" +
                    "Вещественные числа должны иметь вид ##..,##..", "Ошибка", MessageBoxButton.OK);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private Dictionary<double, double> accuracyTime;

        private void testManipulator(int testCount, Point3D endPoint/*, double startAcc, double endAcc*/)
        {
            try
            {
                accuracyTime.Clear();
                
                double timeSum = 0;
                double accuracy = 5;

                //if (endAcc > startAcc)
                //{
                //    double temp = startAcc;
                //    startAcc = endAcc;
                //    endAcc = startAcc;
                //}

                //double startAccCopy = startAcc;

                BackgroundWorker worker = new BackgroundWorker();
                worker.WorkerReportsProgress = true;
                worker.DoWork += (o, ea) =>
                {
                    for (int i = 1; i <= 12; i++)
                    {
                        for (int j = 0; j < testCount; j++)
                        {
                            Dispatcher.Invoke((Action)(() =>
                            {
                               timeSum += makeTest(j, endPoint, accuracy);
                            }));
                        }
                        (o as BackgroundWorker).ReportProgress(i * 100 / 12);
                        accuracyTime.Add(accuracy, timeSum/testCount);
                        accuracy = /*i % 2 == 1 ? accuracy / 5 :*/ accuracy / 2;
                        timeSum = 0;
                    }
                };
                worker.ProgressChanged += (o, ea) =>
                {
                    pbTests.Value = ea.ProgressPercentage;
                    testsPassed.Content = (ea.ProgressPercentage).ToString() + "%";
                };
                worker.RunWorkerCompleted += (o, ea) =>
                {
                    //нарисовать график
                   plotModel = new Plot.TimeVsAccuracyPlotModel(accuracyTime);
                   plot.DataContext = plotModel;
                };

                worker.RunWorkerAsync();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }

        /// <summary>
        /// тестирование руки-манипулятора
        /// </summary>
        /// <param name="testIndex">текущий номер теста</param>
        /// <param name="endPoint">желаемая конечная точка руки-манипулятора</param>
        private double makeTest(int testIndex, Point3D endPoint, double accuracy)
        {
            HandManipulator testManipulator = new HandManipulator(manipulator.LinksCopy);
            InverseKinematicProblem inversKinematicProblem = new InverseKinematicProblem(testManipulator);
            inversKinematicProblem.FindThetas(endPoint, rbHillClimbing.IsChecked.Value
                ? AlgorithmGA.HillClimbing : AlgorithmGA.Standart, accuracy);
            return StatisticsGALib.Statistics.LastExecutionTime;
        }
    }
}
