﻿using System;
using System.Windows;

namespace RoboticArmWpf.TestArm
{
    /// <summary>
    /// Interaction logic for TextWindow.xaml
    /// </summary>
    public partial class TextWindow : Window
    {
        public TextWindow(String text, String windowTitle)
        {
            InitializeComponent();

            textBlock.Text = text;
            this.Title = windowTitle;
        }
    }
}
