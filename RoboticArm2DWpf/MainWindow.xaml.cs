﻿using System;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media.Media3D;
using RoboticArmLib;
using System.Collections.Generic;
using StatisticsGALib;
using System.Windows.Threading;
using System.ComponentModel;
using RoboticArmWpf.ManipulatorParameters;
using System.Windows.Controls;
using RoboticArmWpf.Dynamic;
using System.IO.Ports;

namespace RoboticArmWpf
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private RoboArmSimulator simulator;
        private HandManipulator manipulator;

        private Plot.ErrorPlotModel errorPlotModel;

        public MainWindow()
        {
            InitializeComponent();

            simulator = new RoboArmSimulator();
            initManipulator();
            setListThetaContent();

            errorPlotModel = new Plot.ErrorPlotModel(new List<double> { });
            errorPlot.Visibility = Visibility.Hidden;
        }



        private double accuracy = 0.005;

        /// <summary>
        /// Начальная инициализация руки-манипулятора параметрами Roboarm
        /// </summary>
        private void initManipulator()
        {
            try
            {
                manipulator = new HandManipulator();
                manipulator += new Link(8.0, 120, 0, 180);
                manipulator += new Link(8.0, 320, 270, 450);
                manipulator += new Link(20.0, 300, 270, 450);
                //manipulator += new Link(4.0, 90, 0, 360);
                //manipulator += new Link(4.0, 0, 0, 360);
                //manipulator += new Link(4.0, 0, 0, 360);
                //manipulator += new Link(4.0, 0, 0, 360);
                //manipulator += new Link(4.0, 0, 0, 360);
                //manipulator += new Link(4.0, 0, 0, 360);
                //manipulator += new Link(4.0, 0, 0, 360);
                //manipulator += new Link(4.0, 0, 0, 360);
                //manipulator += new Link(4.0, 0, 0, 360);
                //manipulator += new Link(4.0, 0, 0, 360);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Ошибка");
            }
        }

        /// <summary>
        /// Установить значения углов поворота звеньев в визуальных компонентах
        /// </summary>
        private void setListThetaContent()
        {
            listTheta.Text = "";
            listLinkNumber.Items.Clear();
            for (int i = 0; i < manipulator.Links.Count; i++)
            {
                listLinkNumber.Items.Add(i.ToString());
                listTheta.Text += string.Format("{0,3:F}\n", manipulator.Links[i].Theta);
            }
        }

        /// <summary>
        /// Получить значения углов поворота звеньев манипулятора из визуальных компонентов
        /// </summary>
        /// <returns>Список углов поворота звеньев</returns>
        private List<double> thetasFromListTheta()
        {
            List<double> thetas = new List<double>();
            string[] text = listTheta.Text.Split('\n', ' ');
            double theta;
            foreach (string value in text)
            {
                if (double.TryParse(value, out theta))
                    thetas.Add(theta);
            }
            return thetas;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            MainViewport.Camera = simulator.TheCamera;
            MainViewport.Children.Add(simulator.VisualModel(manipulator));
            displayEndPoint();
        }

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            simulator.ChangeCameraPosition(e.Key);
        }


        //отобразить конечную точку манипулятора (точку схвата)
        private void displayEndPoint()
        {
            valueX.Text = string.Format("{0:0.000}", manipulator.EndPoint3D.X);
            valueY.Text = string.Format("{0:0.000}", manipulator.EndPoint3D.Y);
            valueZ.Text = string.Format("{0:0.000}", manipulator.EndPoint3D.Z);
        }

        /// <summary>
        /// обновить состояние модели руки-манипулятора
        /// </summary>
        private void changeSimulatorState()
        {
            MainViewport.Children.Clear();
            MainViewport.Children.Add(simulator.VisualModel(manipulator));
            displayEndPoint();
            setListThetaContent();
            statistiscBlock.Visibility = Visibility.Hidden;
            errorPlot.Visibility = Visibility.Hidden;
            MenuItemErrorPlot.IsChecked = false;
        }

        InverseKinematicProblem inverseKinematic;

        private Point3D getEnteredPoint()
        {
            //получить заданную конечную точку из визуальных компонентов
            double x = Double.Parse(valueX_Copy.Text);
            double y = Double.Parse(valueY_Copy.Text);
            double z = Double.Parse(valueZ_Copy.Text);

            return new Point3D(x, y, z);
        }


        //найти решение обратной задачи кинематики
        private void buttonFind_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                busyIndicator.IsBusy = true;

                AlgorithmGA algorithm = AlgorithmGA.Standart;
                if (rbHillClimbing.IsChecked.Value)
                    algorithm = AlgorithmGA.HillClimbing;
                List<Point3D> points = new List<Point3D>();

                Point3D enteredPoint = getEnteredPoint();

                BackgroundWorker worker = new BackgroundWorker();
                worker.DoWork += (o, ea) =>
                {
                    inverseKinematic = new InverseKinematicProblem(manipulator);
                    points = inverseKinematic.Solve(enteredPoint, algorithm, accuracy);

                    Dispatcher.Invoke((Action)(() =>
                    {
                        if (roboArmWindow != null && roboArmWindow.IsLoaded)
                        {
                            List<double> thetas = new List<double>();
                            thetas.Add(manipulator.Links[0].RotatingAngle);
                            thetas.AddRange(manipulator.Thetas);

                            roboArmWindow.SendToRoboArm(thetas);
                        }

                        drawWithEndPoint(points);
                        displayEndPoint();
                        setListThetaContent();

                        statistiscBlock.Visibility = Visibility.Visible;
                        setStatisticsInfo();
                    }));
                };
                worker.RunWorkerCompleted += (o, ea) =>
                {
                    busyIndicator.IsBusy = false;

                    errorPlotModel = new Plot.ErrorPlotModel(Statistics.ErrorsGA);
                    errorPlot.DataContext = errorPlotModel;
                    MenuItemErrorPlot.IsChecked = true;                   
                };

                busyIndicator.IsBusy = true;
                worker.RunWorkerAsync();
            }
            catch (FormatException ex)
            {
                System.Windows.MessageBox.Show(ExceptionResource.WrongFloatingNumberFormat);
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show(ex.Message);
            }
        }

        private void setStatisticsInfo()
        {
            statistiscBlock.Text = Statistics.IterationsStatistics();
        }

        private void statisticInfoVisible(object sender, MouseButtonEventArgs e)
        {
            if (statistiscBlock.Visibility == Visibility.Visible)
                statistiscBlock.Visibility = Visibility.Hidden;
            else
                statistiscBlock.Visibility = Visibility.Visible;
        }

        //отобразить манипулятор с полученными и заданной конечной точкой
        private void drawWithEndPoint(List<Point3D> points)
        {
            MainViewport.Children.Clear();
            try
            {
                Point3D enteredPoint = getEnteredPoint();
                points.Add(enteredPoint);
                MainViewport.Children.Add(simulator.VisualModel(manipulator, points));
            }
            catch (FormatException ex)
            {
                System.Windows.MessageBox.Show(ExceptionResource.WrongFloatingNumberFormat);
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show(ex.Message);
            }
        }

        private void WindowSizeChanged(object sender, SizeChangedEventArgs e)
        {
            menu.Width = e.NewSize.Width;
            if (this.WindowState == WindowState.Maximized)
            {
                Grid.SetColumn(errorPlot, 1);
                Grid.SetRow(errorPlot, 1);
                errorPlot.Margin = new Thickness { Top = 470, Right = 200 };
            }
            else
            {
                Grid.SetColumn(errorPlot, 2);
                Grid.SetRow(errorPlot, 1);
                errorPlot.Margin = new Thickness { Bottom = 10, Left = 171, Top = 286, Right = 10 };
            }
        }

        private TestArm.TestWindow testWindow;
        private void MenuItemTest_Click(object sender, RoutedEventArgs e)
        {
            if (testWindow != null && testWindow.IsLoaded)
                testWindow.Close();

            testWindow = new TestArm.TestWindow(manipulator.LinksCopy);
            testWindow.Show();
        }

        private void MenuItemTestAccuracy_Click(object sender, RoutedEventArgs e)
        {
            TestArm.TestTimeAccuracy testWindow = new TestArm.TestTimeAccuracy(manipulator.LinksCopy);
            testWindow.Show();
        }

        private void MenuItemParameters_Click(object sender, RoutedEventArgs e)
        {
            ManipulatorParametersWindow parameters = new ManipulatorParametersWindow(manipulator.LinksCopy, accuracy);
            parameters.Closing += (o, ea) =>
            {
                manipulator = parameters.Manipulator;
                accuracy = parameters.Accurasy;
                changeSimulatorState();
            };
            parameters.ShowDialog();
        }

        private void MenuItemEndPoint_Checked(object sender, RoutedEventArgs e)
        {
            setEndPointVisible(true);
        }

        private void MenuItemEndPoint_Unchecked(object sender, RoutedEventArgs e)
        {
            setEndPointVisible(false);
        }

        private void setEndPointVisible(bool visible)
        {
            if (visible)
            {
                groupBoxEndPoint.Visibility = Visibility.Visible;
                valueX.Visibility = Visibility.Visible;
                valueY.Visibility = Visibility.Visible;
                valueZ.Visibility = Visibility.Visible;
                labelX.Visibility = Visibility.Visible;
                labelY.Visibility = Visibility.Visible;
                labelZ.Visibility = Visibility.Visible;
            }
            else
            {
                groupBoxEndPoint.Visibility = Visibility.Hidden;
                valueX.Visibility = Visibility.Hidden;
                valueY.Visibility = Visibility.Hidden;
                valueZ.Visibility = Visibility.Hidden;
                labelX.Visibility = Visibility.Hidden;
                labelY.Visibility = Visibility.Hidden;
                labelZ.Visibility = Visibility.Hidden;
            }
        }

        private void MenuItemThetas_Unchecked(object sender, RoutedEventArgs e)
        {
            setCurrentThetasVisible(false);
        }

        private void MenuItemThetas_Checked(object sender, RoutedEventArgs e)
        {
            setCurrentThetasVisible(true);
        }

        private void setCurrentThetasVisible(bool visible)
        {
            if (visible)
            {
                groupBoxThetas.Visibility = Visibility.Visible;
                listLinkNumber.Visibility = Visibility.Visible;
                listTheta.Visibility = Visibility.Visible;
            }
            else
            {
                groupBoxThetas.Visibility = Visibility.Hidden;
                listLinkNumber.Visibility = Visibility.Hidden;
                listTheta.Visibility = Visibility.Hidden;
            }
        }


        private void MenuItemErrorPlot_Unchecked(object sender, RoutedEventArgs e)
        {
            errorPlot.Visibility = Visibility.Hidden;
        }

        private void MenuItemErrorPlot_Checked(object sender, RoutedEventArgs e)
        {
            errorPlot.Visibility = Visibility.Visible;
        }

        private void TitleBar_MouseDown(object sender, MouseButtonEventArgs e)
        {
            this.DragMove();
        }

        private void CloseIcon_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            this.Close();
        }


        private DynamicPlotsWindow plotsWindow;

        /// <summary>
        /// движение по заданной траетории
        /// </summary>
        private void moveAlongTrajectory(Trajectory trajectory, Point3D start, Point3D end, int pointsCount, bool drawStates)
        {
            try
            {
                errorPlot.Visibility = Visibility.Hidden;
                
                List<Point3D> pointslist = new List<Point3D>();
                inverseKinematic = new InverseKinematicProblem(manipulator);
                double linkRadius = 0.05;
                
                List<Point3D> lineToPoints;

                if (trajectory == Trajectory.Infinity)
                    lineToPoints = DataConverterLib.TrajectoryConverter.InfinitySymbolToPoints(start, pointsCount);
                else
                    lineToPoints = DataConverterLib.TrajectoryConverter.LineToPoints(start, end, pointsCount);

                List<List<double>> findedThetas = new List<List<double>>();

                BackgroundWorker worker = new BackgroundWorker();
                worker.DoWork += (o, ea) =>
                {
                    findedThetas = inverseKinematic.SolveDynamic(lineToPoints, accuracy);
                };
                worker.RunWorkerCompleted += (o, ea) =>
                {
                    busyIndicator.IsBusy = false;
                    MainViewport.Children.Clear();
                    foreach (List<double> result in findedThetas)
                    {
                        manipulator.AddRotatingAngle(result[0]);
                        manipulator.ChangeThetas(result.GetRange(1, manipulator.Links.Count));
                        manipulator.Rotate();
                        pointslist.Add(manipulator.EndPoint3D);

                        if (drawStates)                        
                            MainViewport.Children.Add(simulator.VisualModel(manipulator, linkRadius));
                    }
                    MainViewport.Children.Add(simulator.VisualModel(manipulator, pointslist));
                    displayEndPoint();
                    setListThetaContent();
                    setStatisticsInfo();
                    statistiscBlock.Visibility = Visibility.Visible;

                    if (plotsWindow != null)
                        plotsWindow.Close();

                    plotsWindow = new DynamicPlotsWindow(findedThetas);
                    plotsWindow.Show();

                    if (roboArmWindow != null && roboArmWindow.IsLoaded)
                    {
                        roboArmWindow.MoveRoboArm(findedThetas);
                    }
                };

                busyIndicator.IsBusy = true;
                worker.RunWorkerAsync();
            }
            catch (FormatException ex)
            {
                System.Windows.MessageBox.Show(ExceptionResource.WrongFloatingNumberFormat);
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show(ex.Message);
            }
        }
              

        private void MenuItemMovement_Click(object sender, RoutedEventArgs e)
        {
            Point3D start = new Point3D();
            Point3D end = new Point3D();
            int pointsCount = 0;
            bool drawStates = false;
            Trajectory trajectory = Trajectory.Line;

            MovementParametersWindow movementWindow = new MovementParametersWindow();
            movementWindow.Closing += (o, ea) =>
            {
                start = movementWindow.StartPoint;
                end = movementWindow.EndPoint;
                pointsCount = movementWindow.PointsCount;
                drawStates = movementWindow.DrawAllStates;
                trajectory = movementWindow.CurrentTrajectory;   
            };
            movementWindow.Closed += (o, ea) =>
            {
                if (trajectory != Trajectory.None && movementWindow.DrawChecked)
                    moveAlongTrajectory(trajectory,start,end,pointsCount,drawStates);
            };

            movementWindow.ShowDialog();
        }
        
        RoboArm.RoboArmWindow roboArmWindow;

        private void MenuItemPortSettings_Click(object sender, RoutedEventArgs e)
        {
            SerialPort serialPort = new SerialPort();
            RoboArm.PortSettings portSettings = new RoboArm.PortSettings();

            portSettings.Closing += (o, ea) =>
            {
                serialPort = portSettings.serialPort;
            };
            portSettings.Closed += (o, ea) =>
            {
                initManipulator();

                roboArmWindow = new RoboArm.RoboArmWindow(serialPort);
                roboArmWindow.Show();
            };

            portSettings.ShowDialog();
        }

        private void Window_Closing(object sender, CancelEventArgs e)
        {
            if (roboArmWindow != null && roboArmWindow.IsEnabled)
                roboArmWindow.Close();

            if (plotsWindow != null && plotsWindow.IsEnabled)
                plotsWindow.Close();

            if (testWindow != null && testWindow.IsEnabled)
                testWindow.Close();
        }
    }
}
