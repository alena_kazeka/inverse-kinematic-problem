﻿using System.Collections.Generic;
using System.Windows;

namespace RoboticArmWpf.Dynamic
{
    /// <summary>
    /// Interaction logic for DynamicPlotsWindow.xaml
    /// </summary>
    public partial class DynamicPlotsWindow : Window
    {
        public DynamicPlotsWindow(List<List<double>> thetas)
        {
            InitializeComponent();

            dynamicPlots = new Plot.DynamicPlotModel[4];
            drawDynamicPlots(thetas);
        }

        private Plot.DynamicPlotModel[] dynamicPlots;
        

        private void drawDynamicPlots(List<List<double>> findedThetas)
        {
            List<List<double>> thetas = new List<List<double>>();
            for (int i = 0; i < 4; i++)
            {
                List<double> tempThetas = new List<double>();
                for (int j = 0; j < findedThetas.Count; j++)
                {
                    if (findedThetas[j].Count != 4)
                        this.Close();
                    tempThetas.Add(findedThetas[j][i]);
                }
                thetas.Add(tempThetas);
            }

            dynamicPlots[0] = new Plot.DynamicPlotModel(thetas[0], 0);
            dynamicPlotRotation.DataContext = dynamicPlots[0];

            dynamicPlots[1] = new Plot.DynamicPlotModel(thetas[1], 1);
            dynamicPlotFirstLink.DataContext = dynamicPlots[1];

            dynamicPlots[2] = new Plot.DynamicPlotModel(thetas[2], 2);
            dynamicPlotSecondLink.DataContext = dynamicPlots[2];

            dynamicPlots[3] = new Plot.DynamicPlotModel(thetas[3], 3);
            dynamicPlotThirdLink.DataContext = dynamicPlots[3];
        }
    }
}
