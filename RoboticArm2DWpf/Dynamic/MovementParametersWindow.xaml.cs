﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Media3D;

namespace RoboticArmWpf.Dynamic
{
    public enum Trajectory
    {
        Line,
        Infinity,
        None
    }

    /// <summary>
    /// Interaction logic for MovementParameters.xaml
    /// </summary>
    public partial class MovementParametersWindow : Window
    {
        public MovementParametersWindow()
        {
            InitializeComponent();
            chbShowStates.IsChecked = false;

            CurrentTrajectory = Trajectory.None;
        }

        private void rbLine_Checked(object sender, RoutedEventArgs e)
        {
            setStartPointEnable(true);
            setEndPointEnable(true);
            comboBoxCount.IsEnabled = true;
            buttonStart.IsEnabled = true;
            chbShowStates.IsEnabled = true;

            CurrentTrajectory = Trajectory.Line;
        }

        private void rbInfinity_Checked(object sender, RoutedEventArgs e)
        {
            setEndPointEnable(false);
            setStartPointEnable(true);
            comboBoxCount.IsEnabled = true;
            buttonStart.IsEnabled = true;
            chbShowStates.IsEnabled = true;

            CurrentTrajectory = Trajectory.Infinity;
        }

        private void setStartPointEnable(bool enable)
        {
            valueX_start.IsEnabled = enable;
            valueY_start.IsEnabled = enable;
            valueZ_start.IsEnabled = enable;
        }

        private void setEndPointEnable(bool enable)
        {
            valueX_end.IsEnabled = enable;
            valueY_end.IsEnabled = enable;
            valueZ_end.IsEnabled = enable;
        }

        public Point3D StartPoint
        {
            get;
            private set;
        }

        public Point3D EndPoint
        {
            get;
            private set;
        }

        public Trajectory CurrentTrajectory
        {
            get;
            private set;
        }
        
        public bool DrawAllStates
        {
            get;
            private set;
        }

        public int PointsCount
        {
            get;
            private set;
        }

        public bool DrawChecked = false;

        private void buttonStart_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (CurrentTrajectory == Trajectory.None)
                    this.Close();

                double x_start = Double.Parse(valueX_start.Text);
                double y_start = Double.Parse(valueY_start.Text);
                double z_start = Double.Parse(valueZ_start.Text);

                StartPoint = new Point3D(x_start, y_start, z_start);

                if (CurrentTrajectory == Trajectory.Line)
                {
                    double x_end = Double.Parse(valueX_end.Text);
                    double y_end = Double.Parse(valueY_end.Text);
                    double z_end = Double.Parse(valueZ_end.Text);

                    EndPoint = new Point3D(x_end, y_end, z_end);
                }

                DrawAllStates = chbShowStates.IsChecked.Value;
                ComboBoxItem typeItem = (ComboBoxItem)comboBoxCount.SelectedItem;
                PointsCount = Int32.Parse(typeItem.Content.ToString());
                DrawChecked = true;
                this.Close();
            }
            catch (FormatException ex)
            {
                System.Windows.MessageBox.Show(ExceptionResource.WrongFloatingNumberFormat);
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show(ex.Message);
            }
        }
    }
}
