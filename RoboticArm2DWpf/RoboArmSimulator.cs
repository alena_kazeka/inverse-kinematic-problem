﻿using System;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Media3D;
using RoboticArmLib;
using System.Collections.Generic;

namespace RoboticArmWpf
{
    public class RoboArmSimulator
    {
        public RoboArmSimulator()
        {
            mainModel3Dgroup = new Model3DGroup();
            // Give the camera its initial position.
            TheCamera = new PerspectiveCamera();
            TheCamera.FieldOfView = 90;
            positionCamera();
        }

        // The main object model group.
        private Model3DGroup mainModel3Dgroup;

        // The camera.
        public PerspectiveCamera TheCamera;

        // The camera's current location.
        private double cameraPhi = Math.PI / 6.0;       // 30 degrees
        private double cameraTheta = Math.PI / 6.0;     // 30 degrees
        private double cameraR = 50.0;

        // The change in CameraPhi when you press the up and down arrows.
        private const double CAMERA_DPhi = 0.1;

        // The change in CameraTheta when you press the left and right arrows.
        private const double CAMERA_DTheta = 0.1;

        // The change in CameraR when you press + or -.
        private const double CAMERA_DR = 0.1;

        // Define the lights.
        private void defineLights()
        {
            AmbientLight ambient_light = new AmbientLight(Colors.Gray);
            DirectionalLight directional_light =
                new DirectionalLight(Colors.Gray, new Vector3D(-1.0, -3.0, -2.0));
            mainModel3Dgroup.Children.Add(ambient_light);
            mainModel3Dgroup.Children.Add(directional_light);
        }

        private void addTriangle(MeshGeometry3D mesh, Point3D point1, Point3D point2, Point3D point3)
        {
            // Create the points.
            int index1 = mesh.Positions.Count;
            mesh.Positions.Add(point1);
            mesh.Positions.Add(point2);
            mesh.Positions.Add(point3);

            // Create the triangle.
            mesh.TriangleIndices.Add(index1++);
            mesh.TriangleIndices.Add(index1++);
            mesh.TriangleIndices.Add(index1);
        }

        // Add a cylinder.
        private void addCylinder(MeshGeometry3D mesh,
            Point3D end_point, Vector3D axis, double radius, int num_sides)
        {
            // Get two vectors perpendicular to the axis.
            Vector3D v1;
            if ((axis.Z < -0.01) || (axis.Z > 0.01))
                v1 = new Vector3D(axis.Z, axis.Z, -axis.X - axis.Y);
            else
                v1 = new Vector3D(-axis.Y - axis.Z, axis.X, axis.X);
            Vector3D v2 = Vector3D.CrossProduct(v1, axis);

            // Make the vectors have length radius.
            v1 *= (radius / v1.Length);
            v2 *= ((radius - 0.1) / v2.Length);

            // Make the top end cap.
            double theta = 0;
            double dtheta = 2 * Math.PI / num_sides;
            for (int i = 0; i < num_sides; i++)
            {
                Point3D p1 = end_point +
                    Math.Cos(theta) * v1 +
                    Math.Sin(theta) * v2;
                theta += dtheta;
                Point3D p2 = end_point +
                    Math.Cos(theta) * v1 +
                    Math.Sin(theta) * v2;
                addTriangle(mesh, end_point, p1, p2);
            }

            // Make the bottom end cap.
            Point3D end_point2 = end_point + axis;
            theta = 0;
            for (int i = 0; i < num_sides; i++)
            {
                Point3D p1 = end_point2 +
                    Math.Cos(theta) * v1 +
                    Math.Sin(theta) * v2;
                theta += dtheta;
                Point3D p2 = end_point2 +
                    Math.Cos(theta) * v1 +
                    Math.Sin(theta) * v2;
                addTriangle(mesh, end_point2, p2, p1);
            }

            // Make the sides.
            theta = 0;
            for (int i = 0; i < num_sides; i++)
            {
                Point3D p1 = end_point +
                    Math.Cos(theta) * v1 +
                    Math.Sin(theta) * v2;
                theta += dtheta;
                Point3D p2 = end_point +
                    Math.Cos(theta) * v1 +
                    Math.Sin(theta) * v2;

                Point3D p3 = p1 + axis;
                Point3D p4 = p2 + axis;

                addTriangle(mesh, p1, p3, p2);
                addTriangle(mesh, /*end_point*/p2, p3, p4);
            }
         
        }

        // Set the vector's length.
        private Vector3D scaleVector(Vector3D vector, double length)
        {
            double scale = length / vector.Length;
            return new Vector3D(
                vector.X * scale,
                vector.Y * scale,
                vector.Z * scale);
        }

        public void ChangeCameraPosition(Key key)
        {
            switch (key)
            {
                case Key.Down:
                    cameraPhi += CAMERA_DPhi;
                    if (cameraPhi > Math.PI / 2.0) cameraPhi = Math.PI / 2.0;
                    break;
                case Key.Up:
                    cameraPhi -= CAMERA_DPhi;
                    if (cameraPhi < -Math.PI / 2.0) cameraPhi = -Math.PI / 2.0;
                    break;
                case Key.Right:
                    cameraTheta -= CAMERA_DTheta;
                    break;
                case Key.Left:
                    cameraTheta += CAMERA_DTheta;
                    break;
                case Key.Add:
                case Key.OemPlus:
                    cameraR -= CAMERA_DR;
                    if (cameraR < CAMERA_DR) cameraR = CAMERA_DR;
                    break;
                case Key.Subtract:
                case Key.OemMinus:
                    cameraR += CAMERA_DR;
                    break;
            }

            // Update the camera's position.
            positionCamera();
        }

        // Position the camera.
        private void positionCamera()
        {
            // Calculate the camera's position in Cartesian coordinates.
            double y = cameraR * Math.Sin(cameraPhi);
            double hyp = cameraR * Math.Cos(cameraPhi);
            double x = hyp * Math.Cos(cameraTheta);
            double z = hyp * Math.Sin(cameraTheta);
            TheCamera.Position = new Point3D(x, y, z);

            // Look toward the origin.
            TheCamera.LookDirection = new Vector3D(-x, -y, -z);

            // Set the Up direction.
            TheCamera.UpDirection = new Vector3D(0, 1, 0);
        }

        public ModelVisual3D VisualModel(HandManipulator manipulator, double radius = 0.4)
        {
            mainModel3Dgroup = new Model3DGroup();
            defineLights();
            move(mainModel3Dgroup, manipulator.Links, radius);
     
            // Add the group of models to a ModelVisual3D.
            ModelVisual3D visualModel = new ModelVisual3D();
            visualModel.Content = mainModel3Dgroup;

            return visualModel;
        }

        /// <summary>
        /// Визуальная модель манипулятора с отрисовкой точек, полученных в процессе работы ГА
        /// </summary>
        /// <param name="manipulator">рука-манипулятор</param>
        /// <param name="points">последняя точка - заданная конечная точка, предпоследняя - найденная (результат решения ОКЗ), остальные - лучшие точки в процессе поиска решения</param>
        /// <returns></returns>
        public ModelVisual3D VisualModel(HandManipulator manipulator, List<Point3D> points, double radius = 0.4)
        {
            if (points == null || points.Count == 0)
                return VisualModel(manipulator);


            mainModel3Dgroup = new Model3DGroup();
            defineLights();
            move(mainModel3Dgroup, manipulator.Links, radius);

            Vector3D vector = new Vector3D(0.04, 0.04, 0.04);

            if (points.Count > 2)
            {
                for (int i = 0; i < points.Count - 2; i++)
                {
                    MeshGeometry3D mesh = new MeshGeometry3D();
                    addCylinder(mesh, points[i] - vector / 2, vector, 0.05, 4);
                    SolidColorBrush brush = Brushes.Gold;
                    DiffuseMaterial material = new DiffuseMaterial(brush);
                    GeometryModel3D model = new GeometryModel3D(mesh, material);
                    mainModel3Dgroup.Children.Add(model);
                }
            }

            if (points.Count > 1)
            {
                MeshGeometry3D meshFinded = new MeshGeometry3D();
                addCylinder(meshFinded, points[points.Count - 2] - vector / 2, vector, 0.07, 4);
                SolidColorBrush brushFinded = Brushes.MediumSpringGreen;
                DiffuseMaterial materialFinded = new DiffuseMaterial(brushFinded);
                GeometryModel3D modelFinded = new GeometryModel3D(meshFinded, materialFinded);
                mainModel3Dgroup.Children.Add(modelFinded);
            }
            
            MeshGeometry3D meshLast = new MeshGeometry3D();
            addCylinder(meshLast, points[points.Count - 1] - vector / 2, vector, 0.07, 4);
            SolidColorBrush brushLast = Brushes.Crimson;
            DiffuseMaterial materialLast = new DiffuseMaterial(brushLast);
            GeometryModel3D modelLast = new GeometryModel3D(meshLast, materialLast);
            mainModel3Dgroup.Children.Add(modelLast);

            // Add the group of models to a ModelVisual3D.
            ModelVisual3D visualModel = new ModelVisual3D();
            visualModel.Content = mainModel3Dgroup;

            return visualModel;
        }

        private void move(Model3DGroup model_group, List<Link> links, double radius)
        {
            double prevTheta = 0;
            foreach(Link link in links)
            {
                MeshGeometry3D mesh = new MeshGeometry3D();
                addCylinder(mesh, link.StartPoint, link.RotatedVector(prevTheta), radius, 9);
                SolidColorBrush brush = Brushes.LightSteelBlue;
                DiffuseMaterial material = new DiffuseMaterial(brush);
                GeometryModel3D model = new GeometryModel3D(mesh, material);
                model_group.Children.Add(model);
                prevTheta += link.Theta;
            }        

            MeshGeometry3D meshAxis = new MeshGeometry3D();
            addCylinder(meshAxis, new Point3D(0, 0, 0),
                new Vector3D(0, -0.2, 0), 16, 16);
            SolidColorBrush brushAxis = Brushes.AliceBlue;
            DiffuseMaterial materialAxis = new DiffuseMaterial(brushAxis);
            GeometryModel3D modelAxis = new GeometryModel3D(meshAxis, materialAxis);
            model_group.Children.Add(modelAxis);
        }
    }
}